package legallexcrm.Models;

public class HourlyFeeInnerModel {
	private int matter_no;
	private String activity;
	private String billing_description;
	private String tax;
	private String rate;
	private String units;
	private String amount;
	private String tax_amount;
	private String total_amount;

	public HourlyFeeInnerModel(int matter_no, String activity, String billing_description, String tax, String rate,
			String units, String amount, String tax_amount, String total_amount) {
		super();
		this.matter_no = matter_no;
		this.activity = activity;
		this.billing_description = billing_description;
		this.tax = tax;
		this.rate = rate;
		this.units = units;
		this.amount = amount;
		this.tax_amount = tax_amount;
		this.total_amount = total_amount;
	}

	public int getMatter_no() {
		return matter_no;
	}

	public void setMatter_no(int matter_no) {
		this.matter_no = matter_no;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getBilling_description() {
		return billing_description;
	}

	public void setBilling_description(String billing_description) {
		this.billing_description = billing_description;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTax_amount() {
		return tax_amount;
	}

	public void setTax_amount(String tax_amount) {
		this.tax_amount = tax_amount;
	}

	public String getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}

}
