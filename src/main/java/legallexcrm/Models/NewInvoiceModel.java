package legallexcrm.Models;

public class NewInvoiceModel {

	private int invoice_id;
	private int matter_no;
	private String inv_date;
	private String inv_description;
	private String inv_type;
	private String inv_staff;
	private String inv_rate_each;
	private String inv_hrs_qty;
	private String inv_amount;

	public NewInvoiceModel(int invoice_id, int matter_no, String inv_date, String inv_description, String inv_type,
			String inv_staff, String inv_rate_each, String inv_hrs_qty, String inv_amount) {
		super();
		this.invoice_id = invoice_id;
		this.matter_no = matter_no;
		this.inv_date = inv_date;
		this.inv_description = inv_description;
		this.inv_type = inv_type;
		this.inv_staff = inv_staff;
		this.inv_rate_each = inv_rate_each;
		this.inv_hrs_qty = inv_hrs_qty;
		this.inv_amount = inv_amount;
	}

	public int getInvoice_id() {
		return invoice_id;
	}

	public void setInvoice_id(int invoice_id) {
		this.invoice_id = invoice_id;
	}

	public int getMatter_no() {
		return matter_no;
	}

	public void setMatter_no(int matter_no) {
		this.matter_no = matter_no;
	}

	public String getInv_date() {
		return inv_date;
	}

	public void setInv_date(String inv_date) {
		this.inv_date = inv_date;
	}

	public String getInv_description() {
		return inv_description;
	}

	public void setInv_description(String inv_description) {
		this.inv_description = inv_description;
	}

	public String getInv_type() {
		return inv_type;
	}

	public void setInv_type(String inv_type) {
		this.inv_type = inv_type;
	}

	public String getInv_staff() {
		return inv_staff;
	}

	public void setInv_staff(String inv_staff) {
		this.inv_staff = inv_staff;
	}

	public String getInv_rate_each() {
		return inv_rate_each;
	}

	public void setInv_rate_each(String inv_rate_each) {
		this.inv_rate_each = inv_rate_each;
	}

	public String getInv_hrs_qty() {
		return inv_hrs_qty;
	}

	public void setInv_hrs_qty(String inv_hrs_qty) {
		this.inv_hrs_qty = inv_hrs_qty;
	}

	public String getInv_amount() {
		return inv_amount;
	}

	public void setInv_amount(String inv_amount) {
		this.inv_amount = inv_amount;
	}

	
}
