package legallexcrm.Models;

public class OAClientLedgerModel {
	private int id;
	private int matter_id;
	private String date;
	private String tax_amount;
	private String total_amount;
	private String description;
	private String office_db;
	private String office_cr;
	private String client_db;
	private String client_cr;
	
	public OAClientLedgerModel(int id, int matter_id, String date, String tax_amount, String total_amount,
			String description, String office_db, String office_cr, String client_db, String client_cr) {
		super();
		this.id = id;
		this.matter_id = matter_id;
		this.date = date;
		this.tax_amount = tax_amount;
		this.total_amount = total_amount;
		this.description = description;
		this.office_db = office_db;
		this.office_cr = office_cr;
		this.client_db = client_db;
		this.client_cr = client_cr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMatter_id() {
		return matter_id;
	}

	public void setMatter_id(int matter_id) {
		this.matter_id = matter_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTax_amount() {
		return tax_amount;
	}

	public void setTax_amount(String tax_amount) {
		this.tax_amount = tax_amount;
	}

	public String getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOffice_db() {
		return office_db;
	}

	public void setOffice_db(String office_db) {
		this.office_db = office_db;
	}

	public String getOffice_cr() {
		return office_cr;
	}

	public void setOffice_cr(String office_cr) {
		this.office_cr = office_cr;
	}

	public String getClient_db() {
		return client_db;
	}

	public void setClient_db(String client_db) {
		this.client_db = client_db;
	}

	public String getClient_cr() {
		return client_cr;
	}

	public void setClient_cr(String client_cr) {
		this.client_cr = client_cr;
	}

	
}
