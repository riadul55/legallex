package legallexcrm.Models;
//Office Accounting (OA) VAT Model

public class OAVATModel {

	private int inv_id;
	private int matter_id;
	private String date;
	private String description;
	private String vat_db;
	private String vat_cr;
	
	public OAVATModel(int inv_id, int matter_id, String date, String description, String vat_db, String vat_cr) {
		super();
		this.inv_id = inv_id;
		this.matter_id = matter_id;
		this.date = date;
		this.description = description;
		this.vat_db = vat_db;
		this.vat_cr = vat_cr;
	}

	public int getInv_id() {
		return inv_id;
	}

	public void setInv_id(int inv_id) {
		this.inv_id = inv_id;
	}

	public int getMatter_id() {
		return matter_id;
	}

	public void setMatter_id(int matter_id) {
		this.matter_id = matter_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVat_db() {
		return vat_db;
	}

	public void setVat_db(String vat_db) {
		this.vat_db = vat_db;
	}

	public String getVat_cr() {
		return vat_cr;
	}

	public void setVat_cr(String vat_cr) {
		this.vat_cr = vat_cr;
	}

	
}
