package legallexcrm.Models;

public class DashboardMattersModel {
	private int matter_no;
	private String client;
	private String matter_type;
	private String respondent;
	private String status;
	
	public DashboardMattersModel(int matter_no, String client, String matter_type, String respondent, String status) {
		super();
		this.matter_no = matter_no;
		this.client = client;
		this.matter_type = matter_type;
		this.respondent = respondent;
		this.status = status;
	}

	public int getMatter_no() {
		return matter_no;
	}

	public void setMatter_no(int matter_no) {
		this.matter_no = matter_no;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getMatter_type() {
		return matter_type;
	}

	public void setMatter_type(String matter_type) {
		this.matter_type = matter_type;
	}

	public String getRespondent() {
		return respondent;
	}

	public void setRespondent(String respondent) {
		this.respondent = respondent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
