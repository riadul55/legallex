package legallexcrm.Models;

public class NewOfficePaymentModel {
	private int matter_no;
	private String mdl_nop_description;
	private String mdl_nop_payment_type;
	private String mdl_nop_amount;
	private String mdl_nop_tax;
	private String mdl_nop_tax_amount;
	private String mdl_nop_date;
	private String mdl_nop_total_amount;

	public NewOfficePaymentModel(int matter_no, String mdl_nop_description, String mdl_nop_payment_type,
			String mdl_nop_amount, String mdl_nop_tax, String mdl_nop_tax_amount, String mdl_nop_date,
			String mdl_nop_total_amount) {
		super();
		this.matter_no = matter_no;
		this.mdl_nop_description = mdl_nop_description;
		this.mdl_nop_payment_type = mdl_nop_payment_type;
		this.mdl_nop_amount = mdl_nop_amount;
		this.mdl_nop_tax = mdl_nop_tax;
		this.mdl_nop_tax_amount = mdl_nop_tax_amount;
		this.mdl_nop_date = mdl_nop_date;
		this.mdl_nop_total_amount = mdl_nop_total_amount;
	}

	public int getMatter_no() {
		return matter_no;
	}

	public void setMatter_no(int matter_no) {
		this.matter_no = matter_no;
	}

	public String getMdl_nop_description() {
		return mdl_nop_description;
	}

	public void setMdl_nop_description(String mdl_nop_description) {
		this.mdl_nop_description = mdl_nop_description;
	}

	public String getMdl_nop_payment_type() {
		return mdl_nop_payment_type;
	}

	public void setMdl_nop_payment_type(String mdl_nop_payment_type) {
		this.mdl_nop_payment_type = mdl_nop_payment_type;
	}

	public String getMdl_nop_amount() {
		return mdl_nop_amount;
	}

	public void setMdl_nop_amount(String mdl_nop_amount) {
		this.mdl_nop_amount = mdl_nop_amount;
	}

	public String getMdl_nop_tax() {
		return mdl_nop_tax;
	}

	public void setMdl_nop_tax(String mdl_nop_tax) {
		this.mdl_nop_tax = mdl_nop_tax;
	}

	public String getMdl_nop_tax_amount() {
		return mdl_nop_tax_amount;
	}

	public void setMdl_nop_tax_amount(String mdl_nop_tax_amount) {
		this.mdl_nop_tax_amount = mdl_nop_tax_amount;
	}

	public String getMdl_nop_date() {
		return mdl_nop_date;
	}

	public void setMdl_nop_date(String mdl_nop_date) {
		this.mdl_nop_date = mdl_nop_date;
	}

	public String getMdl_nop_total_amount() {
		return mdl_nop_total_amount;
	}

	public void setMdl_nop_total_amount(String mdl_nop_total_amount) {
		this.mdl_nop_total_amount = mdl_nop_total_amount;
	}

}
