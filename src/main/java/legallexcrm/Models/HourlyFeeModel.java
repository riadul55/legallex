package legallexcrm.Models;

public class HourlyFeeModel {
	private int hfm_matter_no;
	private String hfm_staff;
	private String hfm_billing_stage;
	private String hfm_activity_rate;
	private String hfm_date;
	private String hfm_hearing_type;
	private String hfm_attended_on;
	private String hfm_activity;
	private String hfm_billing_description;
	private String hfm_tax;
	private String hfm_rate;
	private String hfm_units;
	private String hfm_amount;
	private String hfm_tax_amount;
	private String hfm_total_amount;

	public HourlyFeeModel(int hfm_matter_no, String hfm_staff, String hfm_billing_stage, String hfm_activity_rate,
			String hfm_date, String hfm_hearing_type, String hfm_attended_on, String hfm_activity,
			String hfm_billing_description, String hfm_tax, String hfm_rate, String hfm_units, String hfm_amount,
			String hfm_tax_amount, String hfm_total_amount) {
		super();
		this.hfm_matter_no = hfm_matter_no;
		this.hfm_staff = hfm_staff;
		this.hfm_billing_stage = hfm_billing_stage;
		this.hfm_activity_rate = hfm_activity_rate;
		this.hfm_date = hfm_date;
		this.hfm_hearing_type = hfm_hearing_type;
		this.hfm_attended_on = hfm_attended_on;
		this.hfm_activity = hfm_activity;
		this.hfm_billing_description = hfm_billing_description;
		this.hfm_tax = hfm_tax;
		this.hfm_rate = hfm_rate;
		this.hfm_units = hfm_units;
		this.hfm_amount = hfm_amount;
		this.hfm_tax_amount = hfm_tax_amount;
		this.hfm_total_amount = hfm_total_amount;
	}

	public int getHfm_matter_no() {
		return hfm_matter_no;
	}

	public void setHfm_matter_no(int hfm_matter_no) {
		this.hfm_matter_no = hfm_matter_no;
	}

	public String getHfm_staff() {
		return hfm_staff;
	}

	public void setHfm_staff(String hfm_staff) {
		this.hfm_staff = hfm_staff;
	}

	public String getHfm_billing_stage() {
		return hfm_billing_stage;
	}

	public void setHfm_billing_stage(String hfm_billing_stage) {
		this.hfm_billing_stage = hfm_billing_stage;
	}

	public String getHfm_activity_rate() {
		return hfm_activity_rate;
	}

	public void setHfm_activity_rate(String hfm_activity_rate) {
		this.hfm_activity_rate = hfm_activity_rate;
	}

	public String getHfm_date() {
		return hfm_date;
	}

	public void setHfm_date(String hfm_date) {
		this.hfm_date = hfm_date;
	}

	public String getHfm_hearing_type() {
		return hfm_hearing_type;
	}

	public void setHfm_hearing_type(String hfm_hearing_type) {
		this.hfm_hearing_type = hfm_hearing_type;
	}

	public String getHfm_attended_on() {
		return hfm_attended_on;
	}

	public void setHfm_attended_on(String hfm_attended_on) {
		this.hfm_attended_on = hfm_attended_on;
	}

	public String getHfm_activity() {
		return hfm_activity;
	}

	public void setHfm_activity(String hfm_activity) {
		this.hfm_activity = hfm_activity;
	}

	public String getHfm_billing_description() {
		return hfm_billing_description;
	}

	public void setHfm_billing_description(String hfm_billing_description) {
		this.hfm_billing_description = hfm_billing_description;
	}

	public String getHfm_tax() {
		return hfm_tax;
	}

	public void setHfm_tax(String hfm_tax) {
		this.hfm_tax = hfm_tax;
	}

	public String getHfm_rate() {
		return hfm_rate;
	}

	public void setHfm_rate(String hfm_rate) {
		this.hfm_rate = hfm_rate;
	}

	public String getHfm_units() {
		return hfm_units;
	}

	public void setHfm_units(String hfm_units) {
		this.hfm_units = hfm_units;
	}

	public String getHfm_amount() {
		return hfm_amount;
	}

	public void setHfm_amount(String hfm_amount) {
		this.hfm_amount = hfm_amount;
	}

	public String getHfm_tax_amount() {
		return hfm_tax_amount;
	}

	public void setHfm_tax_amount(String hfm_tax_amount) {
		this.hfm_tax_amount = hfm_tax_amount;
	}

	public String getHfm_total_amount() {
		return hfm_total_amount;
	}

	public void setHfm_total_amount(String hfm_total_amount) {
		this.hfm_total_amount = hfm_total_amount;
	}

}
