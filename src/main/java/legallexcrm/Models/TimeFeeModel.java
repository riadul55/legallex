package legallexcrm.Models;

public class TimeFeeModel {
	private String tf_date;
	private String tf_type;
	private String tf_description;
	private String tf_staff;
	private String tf_status;
	private String tf_amount;
		
	public TimeFeeModel(String tf_date, String tf_type, String tf_description, String tf_staff, String tf_status,
			String tf_amount) {
		super();
		this.tf_date = tf_date;
		this.tf_type = tf_type;
		this.tf_description = tf_description;
		this.tf_staff = tf_staff;
		this.tf_status = tf_status;
		this.tf_amount = tf_amount;
	}
	
	public String getTf_date() {
		return tf_date;
	}
	public void setTf_date(String tf_date) {
		this.tf_date = tf_date;
	}
	public String getTf_type() {
		return tf_type;
	}
	public void setTf_type(String tf_type) {
		this.tf_type = tf_type;
	}
	public String getTf_description() {
		return tf_description;
	}
	public void setTf_description(String tf_description) {
		this.tf_description = tf_description;
	}
	public String getTf_staff() {
		return tf_staff;
	}
	public void setTf_staff(String tf_staff) {
		this.tf_staff = tf_staff;
	}
	public String getTf_status() {
		return tf_status;
	}
	public void setTf_status(String tf_status) {
		this.tf_status = tf_status;
	}
	public String getTf_amount() {
		return tf_amount;
	}
	public void setTf_amount(String tf_amount) {
		this.tf_amount = tf_amount;
	}
	
	
}
