package legallexcrm.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import legallexcrm.AutoCompleteComboBoxListener;
import legallexcrm.DBConnection;

public class NMSecondController implements Initializable {

	/*-------------Details----------- */
	@FXML
	private TextField nms_matter_status;

	@FXML
	private TextField nms_matter_no;

	@FXML
	private TextField nms_name;

	@FXML
	private TextField nms_surname;

	/*-------------Staff----------- */
	@FXML
	private ComboBox<String> cmb_staff_credit;

	@FXML
	private ComboBox<String> cmb_staff_p_resp;

	@FXML
	private ComboBox<String> cmb_staff_p_act;

	@FXML
	private ComboBox<String> cmb_staff_p_ass;

	/*-------------Access & Permissions----------- */

	@FXML
	private TextField txt_ref_card;

	@FXML
	private ComboBox<String> cmb_ref_work_obt;

	@FXML
	private ComboBox<String> cmb_ref_rep_group;

	/*-------------Access & Permissions----------- */

	@FXML
	private CheckBox chk_staff_access;

	@FXML
	private CheckBox chk_credit;

	@FXML
	private CheckBox chk_p_resp;

	@FXML
	private CheckBox chk_p_act;

	@FXML
	private CheckBox chk_p_ass;

	@FXML
	private TextField txt_sc_credit;

	@FXML
	private TextField txt_sc_p_resp;

	@FXML
	private TextField txt_sc_p_act;

	@FXML
	private TextField txt_sc_p_ass;

	@FXML
	private TableView tbl_view_add_staff;

	@FXML
	private Label lbl_add_staff;

	@FXML
	private Button btn_staff_add;

	@FXML
	private Button btn_staff_remove;

	/*------------------Matter Title-------------------*/

	@FXML
	private TextArea txt_area_mt_client;

	@FXML
	private TextArea txt_area_mt_others;

	@FXML
	private CheckBox chk_mt_client;

	@FXML
	private CheckBox chk_mt_others;

	/*------------------Accounting-------------------*/
	@FXML
	private TextField txt_fee_est;

	@FXML
	private TextField txt_disb_est;

	@FXML
	private CheckBox chk_fee_est;

	@FXML
	private CheckBox chk_disb_est;

	@FXML
	private TextField tax_fee_est;

	@FXML
	private TextField tax_disb_est;

	/*------------------Accounting Cost Agreement------------------*/

	@FXML
	private CheckBox chk_cost_agr;

	@FXML
	private DatePicker date_cost_agr;

	@FXML
	private CheckBox chk_first_invoice;

	@FXML
	private TextField txt_first_invoice;

	/*------------------Accounting General-------------------------*/

	@FXML
	private ComboBox<String> cmb_df_client_acc;

	@FXML
	private ComboBox<String> cmb_billing_mode;

	@FXML
	private CheckBox chk_tax_free_time;

	@FXML
	private TextArea txt_area_debtor_comment;
	
	@FXML 
	private CheckBox chk_legal_aid;

	@FXML
	private Button btn_nms_ok;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		nms_matter_status.setText("In Progress");
		nms_matter_no.setText("TBA");

		txt_area_mt_client.setText("Your " + NMFirstController.matter_title + " Matter");
		txt_area_mt_others.setText(NMFirstController.matter_title + " Matter With");

		//Adding Items to combo boxes and selecting first option
		cmb_df_client_acc.getItems().add("Default Client Account");
		cmb_df_client_acc.getSelectionModel().select(0);
		cmb_billing_mode.getItems().add("No VAT");
		cmb_billing_mode.getSelectionModel().select(0);

		/*--------Disabling OK Button-----------*/
		btn_nms_ok.setDisable(true);

		/*----------------Listener for Name Field and change OK button status-----------------------*/
		nms_name.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> ov, String t, String t1) {
				if (t1.trim().equals("")) {
					btn_nms_ok.setDisable(true);
				} else {

					btn_nms_ok.setDisable(false);
				}
			}
		});

		listStaff();
		new AutoCompleteComboBoxListener<>(cmb_staff_credit);
		new AutoCompleteComboBoxListener<>(cmb_staff_p_resp);
		new AutoCompleteComboBoxListener<>(cmb_staff_p_act);
		new AutoCompleteComboBoxListener<>(cmb_staff_p_ass);

		/*--------------------------Listeners to Update Staff Control Text field--------------------*/
		cmb_staff_credit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				txt_sc_credit.setText(cmb_staff_credit.getValue().trim());

			}
		});

		cmb_staff_p_resp.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				txt_sc_p_resp.setText(cmb_staff_p_resp.getValue().trim());

			}
		});

		cmb_staff_p_act.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				txt_sc_p_act.setText(cmb_staff_p_act.getValue().trim());

			}
		});

		cmb_staff_p_ass.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				txt_sc_p_ass.setText(cmb_staff_p_ass.getValue().trim());

			}
		});

		/*-------------------------Staff Access Check box Listener-------------------------------*/
		chk_staff_access.setOnAction(event -> {
			if (chk_staff_access.isSelected()) {
				chk_credit.setDisable(false);
				chk_credit.setSelected(true);
				txt_sc_credit.setDisable(false);

				chk_p_resp.setDisable(false);
				chk_p_resp.setSelected(true);
				txt_sc_p_resp.setDisable(false);

				chk_p_act.setDisable(false);
				chk_p_act.setSelected(true);
				txt_sc_p_act.setDisable(false);

				chk_p_ass.setDisable(false);
				chk_p_ass.setSelected(true);
				txt_sc_p_ass.setDisable(false);

				lbl_add_staff.setDisable(false);
				tbl_view_add_staff.setDisable(false);
				btn_staff_add.setDisable(false);
				btn_staff_remove.setDisable(false);

			} else {
				chk_credit.setDisable(true);
				txt_sc_credit.setDisable(true);

				chk_p_resp.setDisable(true);
				txt_sc_p_resp.setDisable(true);

				chk_p_act.setDisable(true);
				txt_sc_p_act.setDisable(true);

				chk_p_ass.setDisable(true);
				txt_sc_p_ass.setDisable(true);

				lbl_add_staff.setDisable(true);
				tbl_view_add_staff.setDisable(true);
				btn_staff_add.setDisable(true);
				btn_staff_remove.setDisable(true);
			}
		});

		/*------------------------Enable Disable Text Area based on Check boxes-------------------------*/
		chk_mt_client.setOnAction(event -> {
			if (chk_mt_client.isSelected()) {
				txt_area_mt_client.setDisable(true);
				txt_area_mt_client.setText("Your " + NMFirstController.matter_title + " Matter");
			} else {
				txt_area_mt_client.setDisable(false);
			}
		});

		chk_mt_others.setOnAction(event -> {
			if (chk_mt_others.isSelected()) {
				txt_area_mt_others.setDisable(true);
				txt_area_mt_others.setText(NMFirstController.matter_title + " Matter With");
			} else {
				txt_area_mt_others.setDisable(false);
			}
		});

		/*----------------Decimal Pattern for Fee Estimates & Disbursements----------------------*/

		Pattern decimalPattern = Pattern.compile("\\d*(\\.\\d{0,2})?");

		UnaryOperator<TextFormatter.Change> decimal = c -> {
			if (decimalPattern.matcher(c.getControlNewText()).matches()) {
				return c;
			} else {
				return null;
			}
		};

		txt_fee_est.setTextFormatter(new TextFormatter<>(decimal));
		txt_disb_est.setTextFormatter(new TextFormatter<>(decimal));
		txt_first_invoice.setTextFormatter(new TextFormatter<>(decimal));

		/*----------------Listener for Fee Estimate and Update Tax-----------------------*/
		txt_fee_est.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> ov, String t, String t1) {
				if (chk_fee_est.isSelected()) {
					if (t1.trim().equals("")) {
						tax_fee_est.clear();
					} else {

						tax_fee_est.setText(Double.toString((Double.parseDouble(t1) * 20) / 100));
					}

				}
			}
		});

		/*----------------Listener for Fee Disbursement and Update Tax-----------------------*/
		txt_disb_est.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> ov, String t, String t1) {
				if (chk_disb_est.isSelected()) {
					if (t1.trim().equals("")) {
						tax_disb_est.clear();
					} else {

						tax_disb_est.setText(Double.toString((Double.parseDouble(t1) * 20) / 100));
					}

				}
			}
		});

		/*------------------------Change Tax Filed for fee estimate based on Check boxes-------------------------*/
		chk_fee_est.setOnAction(event -> {
			if (chk_fee_est.isSelected()) {
				tax_fee_est.setVisible(true);
				if (txt_fee_est.getText().trim().equals("")) {
					tax_fee_est.clear();
				} else {
					tax_fee_est.setText(Double.toString((Double.parseDouble(txt_fee_est.getText().trim()) * 20) / 100));
				}
			} else {
				tax_fee_est.clear();
				tax_fee_est.setVisible(false);
			}
		});

		/*------------------------Change Tax Filed for fee estimate based on Check boxes-------------------------*/
		chk_disb_est.setOnAction(event -> {
			if (chk_disb_est.isSelected()) {
				tax_disb_est.setVisible(true);
				if (txt_disb_est.getText().trim().equals("")) {
					tax_disb_est.clear();
				} else {
					tax_disb_est
							.setText(Double.toString((Double.parseDouble(txt_disb_est.getText().trim()) * 20) / 100));
				}
			} else {
				tax_disb_est.clear();
				tax_disb_est.setVisible(false);
			}
		});

//End of Initialization function
	}

	public void listStaff() {
		ObservableList<String> staffList = FXCollections.observableArrayList();
		staffList.clear();
		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT * from tbl_staff";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				staffList.add(rs.getString("staff_name"));
			}

			cmb_staff_credit.setItems(staffList);
			cmb_staff_credit.getSelectionModel().selectFirst();

			cmb_staff_p_resp.setItems(staffList);
			cmb_staff_p_resp.getSelectionModel().selectFirst();

			cmb_staff_p_act.setItems(staffList);
			cmb_staff_p_act.getSelectionModel().selectFirst();

			cmb_staff_p_ass.setItems(staffList);
			cmb_staff_p_ass.getSelectionModel().selectFirst();

			txt_sc_credit.setText(cmb_staff_credit.getValue().trim());
			txt_sc_p_resp.setText(cmb_staff_p_resp.getValue().trim());
			txt_sc_p_act.setText(cmb_staff_p_act.getValue().trim());
			txt_sc_p_ass.setText(cmb_staff_p_ass.getValue().trim());

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	private void onBtnNMSOkClicked(ActionEvent event) throws IOException {
		int cardId = 0;
		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			Statement statement = conn.createStatement();
			// Get Card Id to insert new card in tbl_cards & tbl_card_person
			
			statement.execute("select MAX(card_id) from tbl_cards WHERE user_id=" + LoginController.id + "");
		
			ResultSet rs = statement.getResultSet();
			if(rs.next())
				cardId= rs.getInt(1)+1;

			statement.execute("INSERT INTO tbl_cards (user_id, card_type, name) VALUES (" + LoginController.id
					+ ",'Person','" + nms_name.getText().trim() + "')");
            statement.execute("INSERT INTO tbl_card_person (card_id,person_name,person_surname) VALUES ("+cardId+",'"+nms_name.getText().trim()+"','"+nms_surname.getText().trim()+"')");

//			String query = "INSERT INTO tbl_matters VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			String query = "INSERT INTO tbl_matters(user_id, matter_name, country, client, other_side, other_side_solicitor, other_side_insurer, chk_matter_description, matter_description, matter_status, matter_no, card_id, staff_credit, staff_p_resp, staff_p_act, staff_p_ass, chk_sc, chk_credit, sc_credit, chk_p_resp, sc_p_resp, chk_p_act, sc_p_act, chk_p_ass, sc_p_ass, ref_card_id, ref_name, mt_client, mt_others, fee_est, chk_fee_est, fee_est_tax, disb_est, chk_disb_est, disb_est_tax, default_client_acc, billing_mode, chk_cost_agr, date_rec, chk_first_invoice, first_invoice_fee, debtor_comment, legal_aid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, LoginController.id);
			pstmt.setString(2, NMFirstController.matter_title);
			pstmt.setString(3, NMFirstController.country);
			pstmt.setString(4, NMFirstController.client);
			pstmt.setString(5, NMFirstController.other_side);
			pstmt.setString(6, NMFirstController.other_side_solicitor);
			pstmt.setString(7, "Not Applicable");
			pstmt.setInt(8, NMFirstController.chk_description);
			pstmt.setString(9, NMFirstController.description);
			pstmt.setString(10, nms_matter_status.getText().trim());
			pstmt.setString(11, "No1");// matter number
			pstmt.setInt(12, cardId);// card id
			pstmt.setString(13, cmb_staff_credit.getSelectionModel().getSelectedItem().toString());
			pstmt.setString(14, cmb_staff_p_resp.getSelectionModel().getSelectedItem().toString());
			pstmt.setString(15, cmb_staff_p_act.getSelectionModel().getSelectedItem().toString());
			pstmt.setString(16, cmb_staff_p_ass.getSelectionModel().getSelectedItem().toString());
			pstmt.setInt(17, chk_staff_access.isSelected() ? 1 : 0);
			pstmt.setInt(18, chk_credit.isSelected() ? 1 : 0);
			pstmt.setString(19, txt_sc_credit.getText().trim());
			pstmt.setInt(20, chk_p_resp.isSelected() ? 1 : 0);
			pstmt.setString(21, txt_sc_p_resp.getText().trim());
			pstmt.setInt(22, chk_p_act.isSelected() ? 1 : 0);
			pstmt.setString(23, txt_sc_p_act.getText().trim());
			pstmt.setInt(24, chk_p_ass.isSelected() ? 1 : 0);
			pstmt.setString(25, txt_sc_p_ass.getText().trim());
			pstmt.setInt(26, 5);// referrer card id
			pstmt.setString(27, "ref name"); // referrer name
			pstmt.setString(28, txt_area_mt_client.getText().trim());
			pstmt.setString(29, txt_area_mt_others.getText().trim());

			pstmt.setString(30, txt_fee_est.getText().trim());
			pstmt.setInt(31, chk_fee_est.isSelected() ? 1 : 0);
			pstmt.setString(32, tax_fee_est.getText().trim());

			pstmt.setString(33, txt_disb_est.getText().trim());
			pstmt.setInt(34, chk_disb_est.isSelected() ? 1 : 0);
			pstmt.setString(35, tax_disb_est.getText().trim());

			pstmt.setString(36, cmb_df_client_acc.getSelectionModel().getSelectedItem().toString());
			pstmt.setString(37, cmb_billing_mode.getSelectionModel().getSelectedItem().toString());
			pstmt.setInt(38, chk_cost_agr.isSelected() ? 1 : 0);
			pstmt.setString(39, date_cost_agr.getValue().toString());
			pstmt.setInt(40, chk_first_invoice.isSelected() ? 1 : 0);
			pstmt.setString(41, txt_first_invoice.getText().trim());
			pstmt.setString(42, txt_area_debtor_comment.getText().trim());
			pstmt.setInt(43, chk_legal_aid.isSelected()?1:0);

			pstmt.execute();

			statement.close();
			pstmt.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/Dashboard.fxml"));
		Parent root = loader.load();
		((Button) event.getSource()).getScene().setRoot(root);

		
//		DashboardController dbc = loader.getController();
//		dbc.dashboard_bp.setCenter(FXMLLoader.load(getClass().getResource("../views/new_matter/nm_second.fxml")));

//		Stage stage = new Stage();
//		stage.initStyle(StageStyle.UNDECORATED);
//		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/DetailsDashboard.fxml"));
//		Scene scene = new Scene(root);
//		stage.setScene(scene);
//		stage.setMaximized(true);
//		stage.show();
	}

}
