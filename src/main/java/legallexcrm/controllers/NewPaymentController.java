package legallexcrm.controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import legallexcrm.DBConnection;
import legallexcrm.Models.NewOfficePaymentModel;

public class NewPaymentController implements Initializable {

	@FXML
	private TextField nop_bank_account;

	@FXML
	private ComboBox<String> nop_cmb_payment_type;

	@FXML
	private DatePicker nop_date;

	@FXML
	private TextField nop_amount;

	@FXML
	private ComboBox<String> nop_cmb_tax;

	@FXML
	private TextField nop_tax_amount;

	@FXML
	private TextArea nop_description;

	@FXML
	private TextField nop_total_amount;

	// ----------Table View Attributes

	@FXML
	private TableView<NewOfficePaymentModel> tv_nop;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_nop_matter_no;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_nop_description;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_nop_payment_type;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_nop_amount;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_nop_tax;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_nop_tax_amount;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_nop_date;

	@FXML
	private TableColumn<NewOfficePaymentModel, String> col_total_amount;

	ObservableList<NewOfficePaymentModel> nopList = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

		nop_date.setValue(LocalDate.now());

		// setting values of combo box tax items
		nop_cmb_payment_type.getItems().addAll("Bank/Cheque", "Credit Card", "Debit Card");
		nop_cmb_payment_type.getSelectionModel().select(0);

		// setting values of combo box tax items
		nop_cmb_tax.getItems().addAll("VAT Included", "No VAT");
		nop_cmb_tax.getSelectionModel().select(0);

		/*----------------Decimal Pattern for Amount----------------------*/

		Pattern decimalPattern = Pattern.compile("\\d*(\\.\\d{0,2})?");

		UnaryOperator<TextFormatter.Change> decimal = c -> {
			if (decimalPattern.matcher(c.getControlNewText()).matches()) {
				return c;
			} else {
				return null;
			}
		};

		nop_amount.setTextFormatter(new TextFormatter<>(decimal));

		/*----------------Listener for Amount------------------------------*/
		nop_amount.textProperty().addListener((observable, oldValue, newValue) -> {

			if (nop_amount.getText().trim().isEmpty() || Integer.parseInt(nop_amount.getText().trim()) == 0) {
				nop_tax_amount.clear();
				nop_total_amount.clear();
			} else {

				if (nop_cmb_tax.getSelectionModel().getSelectedIndex() == 0) {
					nop_tax_amount
							.setText(Double.toString((Double.parseDouble(nop_amount.getText().trim()) * 20) / 100));

					nop_total_amount.setText(Double.toString(Double.parseDouble(nop_amount.getText().trim())
							+ Double.parseDouble(nop_tax_amount.getText().trim())));
				} else {
					nop_tax_amount.clear();
					nop_total_amount.setText(nop_amount.getText().trim());
				}

			}

		});

		/* Combo Box listener for tax status */
		nop_cmb_tax.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
			if (nop_cmb_tax.getSelectionModel().getSelectedIndex() == 1) {
				nop_tax_amount.clear();
				if ((!nop_amount.getText().trim().isEmpty() && Double.parseDouble(nop_amount.getText().trim()) != 0)) {

					nop_total_amount.setText(nop_amount.getText().trim());
				} else {
					nop_total_amount.clear();
					nop_amount.clear();
				}
			} else {
				if ((!nop_amount.getText().trim().isEmpty() && Double.parseDouble(nop_amount.getText().trim()) != 0)) {
					nop_tax_amount
							.setText(Double.toString((Double.parseDouble(nop_amount.getText().trim()) * 20) / 100));

					nop_total_amount.setText(Double.toString(Double.parseDouble(nop_amount.getText().trim())
							+ Double.parseDouble(nop_tax_amount.getText().trim())));

				} else {
					nop_tax_amount.clear();
					nop_total_amount.clear();
				}

			}
		});

		col_nop_matter_no.setCellValueFactory(new PropertyValueFactory<>("matter_no"));
		col_nop_description.setCellValueFactory(new PropertyValueFactory<>("mdl_nop_description"));
		col_nop_payment_type.setCellValueFactory(new PropertyValueFactory<>("mdl_nop_payment_type"));
		col_nop_amount.setCellValueFactory(new PropertyValueFactory<>("mdl_nop_amount"));
		col_nop_tax.setCellValueFactory(new PropertyValueFactory<>("mdl_nop_tax"));
		col_nop_tax_amount.setCellValueFactory(new PropertyValueFactory<>("mdl_nop_tax_amount"));
		col_nop_date.setCellValueFactory(new PropertyValueFactory<>("mdl_nop_date"));
		col_total_amount.setCellValueFactory(new PropertyValueFactory<>("mdl_nop_total_amount"));

	}

	@FXML
	private void onBtnCloseClicked(ActionEvent event) {

		((Node) event.getSource()).getScene().getWindow().hide();

	}

	@FXML
	private void onBtnMinimizeClicked(ActionEvent event) {

		((Stage) ((Button) event.getSource()).getScene().getWindow()).setIconified(true);

	}

	@FXML
	private void onBtnMaximizeClicked(ActionEvent event) {

		if (((Stage) ((Button) event.getSource()).getScene().getWindow()).isMaximized()) {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(false);
		} else {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(true);
		}
	}

	@FXML
	private void onBtnAddPaymentClicked(ActionEvent event) {
		nopList.add(new NewOfficePaymentModel(DashboardController.temp_matter_no, nop_description.getText().trim(),
				nop_cmb_payment_type.getValue().toString(), nop_amount.getText().trim(),
				nop_cmb_tax.getValue().toString(), nop_tax_amount.getText().trim(), nop_date.getValue().toString(),
				nop_total_amount.getText()));
		tv_nop.setItems(nopList);
	}

	@FXML
	private void onBtnNOPSaveClicked(ActionEvent event) {
		Connection conn;
		String query = "";
		try {
			conn = (Connection) new DBConnection().DBConnect();
			Statement stmt = conn.createStatement();
			for (NewOfficePaymentModel nopIterator : nopList) {
				// Insert into tbl_invoices
//				query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr) VALUES ('"
//						+ DashboardController.temp_matter_no + "','" + nop_date.getValue().toString() + "','"
//						+ nop_tax_amount.getText() + "','-','" + nop_description.getText() + "','-','-','-','"
//						+ nop_total_amount.getText().trim() + "')";

				query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr, type) VALUES ('"
						+ DashboardController.temp_matter_no + "','" + nopIterator.getMdl_nop_date() + "','"
						+ nopIterator.getMdl_nop_tax_amount() + "','-','" + nopIterator.getMdl_nop_description()
						+ "','-','-','-','" + nopIterator.getMdl_nop_total_amount() + "','OfficePayment')";

				stmt.execute(query);
				
				
				
				//Update user balance and type as new payment is added
				stmt.execute(
						"SELECT * from tbl_user_account WHERE matter_id=" + DashboardController.temp_matter_no + "");
				ResultSet rs = stmt.getResultSet();

				if (rs.next()) {
					double tempBalance = Double.parseDouble(rs.getString("balance"));
					switch (rs.getString("balance_type")) {
					case "CR": {
						tempBalance = tempBalance + Double.parseDouble(nopIterator.getMdl_nop_total_amount());
						stmt.execute("UPDATE tbl_user_account SET balance='" + tempBalance + "' WHERE matter_id="
								+ DashboardController.temp_matter_no + "");
					}
						break;
					case "DR": {
						tempBalance = tempBalance - Double.parseDouble(nopIterator.getMdl_nop_total_amount());
						if(tempBalance > 0) {
							stmt.execute("UPDATE tbl_user_account SET balance='" + tempBalance + "' WHERE matter_id="
									+ DashboardController.temp_matter_no + "");
						}else if(tempBalance <= 0) {
							tempBalance =tempBalance * -1;
							
							stmt.execute("UPDATE tbl_user_account SET balance='" + tempBalance + "',balance_type = 'CR' WHERE matter_id="
									+ DashboardController.temp_matter_no + "");
						}else {
							
						}
					}
						break;
					default: {

					}
					}
				}

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((Node) event.getSource()).getScene().getWindow().hide();
	}

}