package legallexcrm.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

public class NCCompanyController implements Initializable {
	@FXML
	private ChoiceBox<String> cb_card_type;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		LoadCardType();

		// Event Listener for Card Type Choice Box on Changed Value
		cb_card_type.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {

			if (!(cb_card_type.getSelectionModel().isEmpty())) {
				if (!(newValue.equals("Company"))) {
					try {
						Stage stg = (Stage) cb_card_type.getScene().getWindow();
						FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/Dashboard.fxml"));
						Parent root;
						root = loader.load();
						cb_card_type.getScene().setRoot(root);

						DashboardController dbc = loader.getController();
						dbc.dashboard_bp.setCenter(FXMLLoader.load(getClass().getResource("legallexcrm/views/new_card/"
								+ cb_card_type.getSelectionModel().selectedItemProperty().getValue() + ".fxml")));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

		});

	}
	
	public void LoadCardType() {
		cb_card_type.getItems().clear();
		cb_card_type.getItems().addAll("Person", "Business", "Company", "GovernmentDept", "Trust");
		cb_card_type.getSelectionModel().select(2);
	}

}
