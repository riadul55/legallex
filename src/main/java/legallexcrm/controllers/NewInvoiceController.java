package legallexcrm.controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import legallexcrm.DBConnection;
import legallexcrm.Models.NewInvoiceModel;

public class NewInvoiceController implements Initializable {
	static int temp_id;

	@FXML
	private TextField matter_no;
	
	@FXML
	private TextArea description;

	@FXML
	private DatePicker date;

	@FXML
	private Label lbl_tax_amount;

	@FXML
	private Label lbl_total_amount;
	
	@FXML
	private TableView<NewInvoiceModel> tv_new_invoice;

	@FXML
	private TableColumn<NewInvoiceModel, String> col_date;

	@FXML
	private TableColumn<NewInvoiceModel, String> col_description;

	@FXML
	private TableColumn<NewInvoiceModel, String> col_type;

	@FXML
	private TableColumn<NewInvoiceModel, String> col_staff;

	@FXML
	private TableColumn<NewInvoiceModel, String> col_rate_each;

	@FXML
	private TableColumn<NewInvoiceModel, String> col_hrs_qty;

	@FXML
	private TableColumn<NewInvoiceModel, String> col_amount;

	ObservableList<NewInvoiceModel> invList = FXCollections.observableArrayList();
	ObservableList<NewInvoiceModel> invFixedList = FXCollections.observableArrayList();
	ObservableList<NewInvoiceModel> invHourlyList = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

		matter_no.setText(String.valueOf(DashboardController.temp_matter_no));
		date.setValue(LocalDate.now());

		col_date.setCellValueFactory(new PropertyValueFactory<>("inv_date"));
		col_description.setCellValueFactory(new PropertyValueFactory<>("inv_description"));
		col_type.setCellValueFactory(new PropertyValueFactory<>("inv_type"));
		col_staff.setCellValueFactory(new PropertyValueFactory<>("inv_staff"));
		col_rate_each.setCellValueFactory(new PropertyValueFactory<>("inv_rate_each"));
		col_hrs_qty.setCellValueFactory(new PropertyValueFactory<>("inv_hrs_qty"));
		col_amount.setCellValueFactory(new PropertyValueFactory<>("inv_amount"));

		DisplayInvoiceItems();

	}

	@FXML
	private void onBtnCloseClicked(ActionEvent event) {

		((Node) event.getSource()).getScene().getWindow().hide();

	}

	@FXML
	private void onBtnMinimizeClicked(ActionEvent event) {

		((Stage) ((Button) event.getSource()).getScene().getWindow()).setIconified(true);

	}

	@FXML
	private void onBtnMaximizeClicked(ActionEvent event) {

		if (((Stage) ((Button) event.getSource()).getScene().getWindow()).isMaximized()) {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(false);
		} else {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(true);
		}
	}

	//displaying Invoices Items that are not billed, while clicked on create new Invoice
	public void DisplayInvoiceItems() {

		Connection conn;
		try {
			double time_fee_total=0.0,tax_amount=0.0;
			conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT MAX(id) from tbl_invoices";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			temp_id = 1;
			if (rs.next()) {
				temp_id = rs.getInt(1) + 1;
			}

			query = "SELECT * from tbl_fixed_fee WHERE matter_no='" + DashboardController.temp_matter_no
					+ "' AND invoice_no=0";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				invFixedList.add(new NewInvoiceModel(temp_id, DashboardController.temp_matter_no, rs.getString("date"),
						rs.getString("billing_description"), rs.getString("type"), rs.getString("staff"),
						rs.getString("rate"), rs.getString("quantity"), rs.getString("total")));
				time_fee_total = time_fee_total + Double.parseDouble(rs.getString("amount"));
				tax_amount = tax_amount + Double.parseDouble(rs.getString("tax_amount"));
			}

			query = "SELECT * from tbl_hourly_fee WHERE matter_no='" + DashboardController.temp_matter_no
					+ "' AND invoice_no=0";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				invHourlyList.add(new NewInvoiceModel(temp_id, DashboardController.temp_matter_no, rs.getString("date"),
						rs.getString("billing_description"), rs.getString("type"), rs.getString("staff"),
						rs.getString("rate"), rs.getString("units"), rs.getString("amount")));
				time_fee_total = time_fee_total + Double.parseDouble(rs.getString("amount"));
				tax_amount = tax_amount + Double.parseDouble(rs.getString("tax_amount"));
			}
			invList.addAll(invFixedList);
			invList.addAll(invHourlyList);

			tv_new_invoice.setItems(invList);
			lbl_tax_amount.setText(tax_amount + "");
			lbl_total_amount.setText(time_fee_total + "");

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void onBtnSaveClicked(ActionEvent event) {
		Connection conn;
		try {
			
			conn = (Connection) new DBConnection().DBConnect();
			String query = "UPDATE tbl_hourly_fee SET invoice_no = '"+temp_id+"' WHERE matter_no = '"+DashboardController.temp_matter_no+"'";
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			
			query = "UPDATE tbl_fixed_fee SET invoice_no = '"+temp_id+"' WHERE matter_no = '"+DashboardController.temp_matter_no+"'";
			stmt.executeUpdate(query);
			
			//Insert into tbl_invoices
			query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr) VALUES ('"+DashboardController.temp_matter_no+"','"+date.getValue().toString()+"','"+lbl_tax_amount.getText()+"','"+lbl_total_amount.getText()+"','"+description.getText()+"','"+lbl_total_amount.getText()+"','-','-','-')";
			stmt.execute(query);
			
			if(Double.parseDouble(lbl_tax_amount.getText().trim())>0.0) {
				query = "INSERT INTO tbl_vat(id, matter_id,type, date, description, vat_db, vat_cr) VALUES ('"+temp_id+"','"+DashboardController.temp_matter_no+"','inv','"+date.getValue().toString()+"','"+description.getText()+" VAT"+"','-','"+lbl_tax_amount.getText()+"')";
				stmt.execute(query);
			}
			
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((Node) event.getSource()).getScene().getWindow().hide();
	}

}
