package legallexcrm.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import legallexcrm.DBConnection;
import legallexcrm.Models.OAClientLedgerModel;
import legallexcrm.Models.OAVATModel;
import legallexcrm.Models.TimeFeeModel;

public class DetailsDashboardController implements Initializable {

	@FXML
	private BorderPane dashboard_bp;

	// Time & Fee Tab Pane Table View
	// Attributes----------------------------------------------
	@FXML
	TabPane tabpane_time_fee;

	@FXML
	private TableView<TimeFeeModel> tv_time_fee;

	@FXML
	private TableColumn<TimeFeeModel, String> col_date;

	@FXML
	private TableColumn<TimeFeeModel, String> col_type;

	@FXML
	private TableColumn<TimeFeeModel, String> col_billing_description;

	@FXML
	private TableColumn<TimeFeeModel, String> col_staff;

	@FXML
	private TableColumn<TimeFeeModel, String> col_status;

	@FXML
	private TableColumn<TimeFeeModel, String> col_amount;

	@FXML
	private Label lbl_time_fee_total;

	// Office Account (OA) Client Ledger Table View
	// Details--------------------------------------
	@FXML
	private TableView<OAClientLedgerModel> tv_client_ledger;

	@FXML
	private TableColumn<OAClientLedgerModel, String> col_cl_date;

	@FXML
	private TableColumn<OAClientLedgerModel, String> col_cl_description;

	@FXML
	private TableColumn<OAClientLedgerModel, String> col_cl_office_db;

	@FXML
	private TableColumn<OAClientLedgerModel, String> col_cl_office_cr;

	@FXML
	private TableColumn<OAClientLedgerModel, String> col_cl_client_db;

	@FXML
	private TableColumn<OAClientLedgerModel, String> col_cl_client_cr;
	
	@FXML
	private Label lbl_client_bal;
	
	@FXML
	private Label lbl_office_bal;

	// Vat Ledger Attributes in Office Account Tab Pane

	@FXML
	private TableView<OAVATModel> tv_vat_ledger;

	@FXML
	private TableColumn<OAVATModel, String> col_vat_date;

	@FXML
	private TableColumn<OAVATModel, String> col_vat_description;

	@FXML
	private TableColumn<OAVATModel, String> col_vat_db;

	@FXML
	private TableColumn<OAVATModel, String> col_vat_cr;

	/* fetching & Displaying dynamic values of matter */
	@FXML
	private Label lbl_matter_status;

	@FXML
	private Button btn_person_resp;

	@FXML
	private Button btn_applicant;

	@FXML
	private Button btn_matter_type;

	@FXML
	private Button btn_respondent;

	@FXML
	private Button btn_compliance;

	@FXML
	private Button btn_child;

	@FXML
	private Button btn_counsel;

	@FXML
	private Button btn_court;

	@FXML
	private Button btn_mediator;

	ObservableList<TimeFeeModel> tfList = FXCollections.observableArrayList();
	ObservableList<OAClientLedgerModel> OAClientLedgerList = FXCollections.observableArrayList();
	ObservableList<OAVATModel> OAVATLedgerList = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		try {
			Connection conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT * from tbl_matters WHERE id='" + DashboardController.matter_id + "'";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				System.out.println(rs.getString("matter_name"));
				btn_matter_type.setText(rs.getString("matter_name"));
				lbl_matter_status.setText(rs.getString("matter_status"));
				btn_person_resp.setText(rs.getString("staff_p_resp"));

				stmt.executeQuery("SELECT name FROM tbl_cards WHERE card_id='" + rs.getInt("card_id") + "'");
				if (stmt.getResultSet().next()) {
					btn_applicant.setText(stmt.getResultSet().getString(1));
				}
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Binding TIME & FEES table View Columns to "TimeFeeModel"
		col_date.setCellValueFactory(new PropertyValueFactory<>("tf_date"));
		col_type.setCellValueFactory(new PropertyValueFactory<>("tf_type"));
		col_billing_description.setCellValueFactory(new PropertyValueFactory<>("tf_description"));
		col_staff.setCellValueFactory(new PropertyValueFactory<>("tf_staff"));
		col_status.setCellValueFactory(new PropertyValueFactory<>("tf_status"));
		col_amount.setCellValueFactory(new PropertyValueFactory<>("tf_amount"));
		// calling function to display items in table
		DisplayTimeFeeTable();

		// Binding Client Ledger Table View Column in Office Accounting Tab Pane to
		// "OAClientLedgerModel"
		col_cl_date.setCellValueFactory(new PropertyValueFactory<>("date"));
		col_cl_description.setCellValueFactory(new PropertyValueFactory<>("description"));
		col_cl_office_db.setCellValueFactory(new PropertyValueFactory<>("office_db"));
		col_cl_office_cr.setCellValueFactory(new PropertyValueFactory<>("office_cr"));
		col_cl_client_db.setCellValueFactory(new PropertyValueFactory<>("client_db"));
		col_cl_client_cr.setCellValueFactory(new PropertyValueFactory<>("client_cr"));

		DisplayOAClientLedger();

		// Binding VAT Ledger Table View Column in Office Accounting Tab Pane to
		// "OAVATModel"
		col_vat_date.setCellValueFactory(new PropertyValueFactory<>("date"));
		col_vat_description.setCellValueFactory(new PropertyValueFactory<>("description"));
		col_vat_db.setCellValueFactory(new PropertyValueFactory<>("vat_db"));
		col_vat_cr.setCellValueFactory(new PropertyValueFactory<>("vat_cr"));

		DisplayOAVATLedger();

		tv_time_fee.getSortOrder().add(col_date);
		tv_client_ledger.getSortOrder().add(col_cl_date);
		tv_vat_ledger.getSortOrder().add(col_vat_date);
	}

	@FXML
	private void onBtnCloseClicked(ActionEvent event) {

		((Node) event.getSource()).getScene().getWindow().hide();

	}

	@FXML
	private void onBtnMinimizeClicked(ActionEvent event) {

		((Stage) ((Button) event.getSource()).getScene().getWindow()).setIconified(true);

	}

	/*----------------------Details & Corresponding------------------------------------------*/

	@FXML
	private void onBtnMaximizeClicked(ActionEvent event) {

		if (((Stage) ((Button) event.getSource()).getScene().getWindow()).isMaximized()) {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(false);
		} else {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(true);
		}
	}

	@FXML
	private void onBtnCreateDocumentClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		System.out.println();
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/CreateDocument.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	@FXML
	private void onBtnNewCommentClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/NewComment.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	@FXML
	private void onBtnNewImportsClicked(ActionEvent event) throws IOException {
		FileChooser fc = new FileChooser();
//		List<File> f = fc.showOpenMultipleDialog(null);
		File file= fc.showOpenDialog(null);
		
	    
	}

	/*-----------------------------------------Time & Fees--------------------------------------------*/

	@FXML
	private void onNewBillableItemClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/NewBillableItem.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	@FXML
	private void onBtnFixedFeeClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/FixedFee.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	/*-----------------------------------------Office Accounting--------------------------------------------*/

	@FXML
	private void onBtnNewPaymentClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/NewPayment.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	@FXML
	private void onBtnNewJournalClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/NewJournal.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	@FXML
	private void onBtnPayAnticipatedClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/PayAnticipated.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	@FXML
	private void onBtnInvoiceLegalAidClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/InvoiceLegalAid.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	@FXML
	private void onBtnInvoiceClicked(ActionEvent event) throws IOException {

		Stage stage = new Stage();
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/Invoice.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}

	public void DisplayTimeFeeTable() {
		tfList.clear();

		Connection conn;
		try {
			double time_fee_total = 0.0;
			conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT * from tbl_hourly_fee WHERE matter_no='" + DashboardController.temp_matter_no
					+ "' AND invoice_no=0";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				tfList.add(new TimeFeeModel(rs.getString("date"), rs.getString("type"),
						rs.getString("billing_description"), rs.getString("staff"), "Billable",
						rs.getString("amount")));
				time_fee_total = time_fee_total + Double.parseDouble(rs.getString("amount"));
			}

			query = "SELECT * from tbl_fixed_fee WHERE matter_no='" + DashboardController.temp_matter_no
					+ "' AND invoice_no=0";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				tfList.add(new TimeFeeModel(rs.getString("date"), rs.getString("type"),
						rs.getString("billing_description"), rs.getString("staff"), "Billable", rs.getString("total")));
				time_fee_total = time_fee_total + Double.parseDouble(rs.getString("total"));
			}

			tv_time_fee.setItems(tfList);
			lbl_time_fee_total.setText("�" + String.format("%.2f", time_fee_total));
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void DisplayOAClientLedger() {
		OAClientLedgerList.clear();

		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT * from tbl_invoices WHERE matter_id='" + DashboardController.temp_matter_no + "'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				OAClientLedgerList.add(new OAClientLedgerModel(rs.getInt("id"), rs.getInt("matter_id"),
						rs.getString("date"), rs.getString("tax_amount"), rs.getString("total_amount"),
						rs.getString("description"), rs.getString("office_db"), rs.getString("office_cr"),
						rs.getString("client_db"), rs.getString("client_cr")));
			}

			// Fetching Vat Ledger Items to be displayed in client ledger
			query = "SELECT * from tbl_vat WHERE matter_id='" + DashboardController.temp_matter_no + "'";
			rs = stmt.executeQuery(query);
			while (rs.next()) {

				if (rs.getString("type").equals("inv")) {
 /*-----------------If type is inv then vat values are reversed------------------------*/
					OAClientLedgerList.add(new OAClientLedgerModel(rs.getInt("id"), rs.getInt("matter_id"),
							rs.getString("date"), "0", "0", rs.getString("description"), rs.getString("vat_cr"),
							rs.getString("vat_db"), "-", "-"));
				} else {

					OAClientLedgerList.add(new OAClientLedgerModel(rs.getInt("id"), rs.getInt("matter_id"),
							rs.getString("date"), "0", "0", rs.getString("description"), rs.getString("vat_db"),
							rs.getString("vat_cr"), "-", "-"));
				}

			}

			tv_client_ledger.setItems(OAClientLedgerList);

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void DisplayOAVATLedger() {
		OAVATLedgerList.clear();

		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT * from tbl_vat WHERE matter_id='" + DashboardController.temp_matter_no + "'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				OAVATLedgerList.add(new OAVATModel(rs.getInt("id"), rs.getInt("matter_id"), rs.getString("date"),
						rs.getString("description"), rs.getString("vat_db"), rs.getString("vat_cr")));

			}

			tv_vat_ledger.setItems(OAVATLedgerList);

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
