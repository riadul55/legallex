package legallexcrm.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;
import legallexcrm.DBConnection;

public class NewFeeController implements Initializable {

	@FXML
	private TextField ff_matter_id;

	@FXML
	private ComboBox<String> ff_activity_code;

	@FXML
	private TextField ff_quantity;

	@FXML
	private TextField ff_rate;

	@FXML
	private DatePicker ff_date;

	@FXML
	private ComboBox<String> ff_cmb_ledger_entry;

	@FXML
	private TextField ff_amount;

	@FXML
	private ComboBox<String> ff_cmb_tax;

	@FXML
	private TextField ff_tax;

	@FXML
	private TextField ff_total;

	@FXML
	private TextArea ff_description;

	@FXML
	private ComboBox<String> ff_cmb_staff;

	@FXML
	private ComboBox<String> ff_cmb_billing;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		ff_matter_id.setText(String.valueOf(DashboardController.temp_matter_no));
		ff_date.setValue(LocalDate.now());

		ff_activity_code.getItems().add("TRL");
		ff_activity_code.getSelectionModel().select(0);

		// setting values of combo box tax items
		ff_cmb_tax.getItems().addAll("VAT Included", "No VAT");
		ff_cmb_tax.getSelectionModel().select(0);

		// setting values of Ledger Entries
		ff_cmb_ledger_entry.getItems().addAll("Office-DR", "Office-CR", "Client-DR", "Client-CR");
		ff_cmb_ledger_entry.getSelectionModel().select(0);

		// displaying staff combo box values
		listStaff();

		// setting values for combo box billing mode
		ff_cmb_billing.getItems().addAll("Billable", "Not Billable");
		ff_cmb_billing.getSelectionModel().select(0);

		/*----------------Decimal Pattern for Fee rates, amount, tax, total----------------------*/

		Pattern decimalPattern = Pattern.compile("\\d*(\\.\\d{0,2})?");

		UnaryOperator<TextFormatter.Change> decimal = c -> {
			if (decimalPattern.matcher(c.getControlNewText()).matches()) {
				return c;
			} else {
				return null;
			}
		};

		ff_rate.setTextFormatter(new TextFormatter<>(decimal));
		ff_amount.setTextFormatter(new TextFormatter<>(decimal));
		ff_tax.setTextFormatter(new TextFormatter<>(decimal));
		ff_total.setTextFormatter(new TextFormatter<>(decimal));

		/*-----------Integer formatter for Quantity-------------------------*/
		ff_quantity.setTextFormatter(new TextFormatter<>(c -> {
			if (!c.getControlNewText().matches("\\d*"))
				return null;
			else
				return c;
		}));
		/*----------------Listener for Quantity only whole numbers------------------------------*/
		ff_quantity.textProperty().addListener((observable, oldValue, newValue) -> {

			if (ff_quantity.getText().trim().isEmpty() || Integer.parseInt(ff_quantity.getText().trim()) == 0) {
				ff_amount.clear();
				ff_tax.clear();
				ff_total.clear();
			} else {
				if (!ff_rate.getText().trim().isEmpty() && Integer.parseInt(ff_rate.getText().trim()) != 0) {
					ff_amount.setText(Double.toString(Integer.parseInt(ff_quantity.getText().trim())
							* Double.parseDouble(ff_rate.getText().trim())));
					if (ff_cmb_tax.getSelectionModel().getSelectedIndex() == 0) {
						ff_tax.setText(Double.toString((Double.parseDouble(ff_amount.getText().trim()) * 20) / 100));
						ff_total.setText(Double.toString((Double.parseDouble(ff_amount.getText().trim())
								+ Double.parseDouble(ff_tax.getText().trim()))));
					} else {
						ff_total.setText(ff_amount.getText().trim());
					}
				}
			}
		});

		ff_rate.textProperty().addListener((observable, oldValue, newValue) -> {

			if (ff_rate.getText().trim().isEmpty() || Integer.parseInt(ff_rate.getText().trim()) == 0) {
				ff_amount.clear();
				ff_tax.clear();
				ff_total.clear();
			} else {
				if (!ff_quantity.getText().trim().isEmpty() && Integer.parseInt(ff_quantity.getText().trim()) != 0) {
					ff_amount.setText(Double.toString(Integer.parseInt(ff_quantity.getText().trim())
							* Double.parseDouble(ff_rate.getText().trim())));
					if (ff_cmb_tax.getSelectionModel().getSelectedIndex() == 0) {
						ff_tax.setText(Double.toString((Double.parseDouble(ff_amount.getText().trim()) * 20) / 100));
						ff_total.setText(Double.toString((Double.parseDouble(ff_amount.getText().trim())
								+ Double.parseDouble(ff_tax.getText().trim()))));
					} else {
						ff_total.setText(ff_amount.getText().trim());
					}
				}
			}

		});

		/* Combo Box listener for tax status */
		ff_cmb_tax.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
			if (ff_cmb_tax.getSelectionModel().getSelectedIndex() == 1) {
				ff_tax.clear();
				if ((!ff_quantity.getText().trim().isEmpty() && Integer.parseInt(ff_quantity.getText().trim()) != 0)
						&& (!ff_rate.getText().trim().isEmpty() && Integer.parseInt(ff_rate.getText().trim()) != 0)) {
					ff_amount.setText(Double.toString(Integer.parseInt(ff_quantity.getText().trim())
							* Double.parseDouble(ff_rate.getText().trim())));
					ff_total.setText(ff_amount.getText());
				} else {
					ff_total.clear();
					ff_amount.clear();
				}
			} else {
				if ((!ff_quantity.getText().trim().isEmpty() && Integer.parseInt(ff_quantity.getText().trim()) != 0)
						&& (!ff_rate.getText().trim().isEmpty() && Integer.parseInt(ff_rate.getText().trim()) != 0)) {
					ff_amount.setText(Double.toString(Integer.parseInt(ff_quantity.getText().trim())
							* Double.parseDouble(ff_rate.getText().trim())));
					ff_tax.setText(Double.toString((Double.parseDouble(ff_amount.getText().trim()) * 20) / 100));
					ff_total.setText(Double.toString((Double.parseDouble(ff_amount.getText().trim())
							+ Double.parseDouble(ff_tax.getText().trim()))));

				} else {
					ff_total.clear();
					ff_amount.clear();
					ff_tax.clear();
				}

			}
		});

	}

	@FXML
	private void onBtnCloseClicked(ActionEvent event) {

		((Node) event.getSource()).getScene().getWindow().hide();

	}

	@FXML
	private void onBtnMinimizeClicked(ActionEvent event) {

		((Stage) ((Button) event.getSource()).getScene().getWindow()).setIconified(true);

	}

	@FXML
	private void onBtnMaximizeClicked(ActionEvent event) {

		if (((Stage) ((Button) event.getSource()).getScene().getWindow()).isMaximized()) {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(false);
		} else {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(true);
		}
	}

	public void listStaff() {
		ObservableList<String> staffList = FXCollections.observableArrayList();
		staffList.clear();
		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT * from tbl_staff";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				staffList.add(rs.getString("staff_name"));
			}

			ff_cmb_staff.setItems(staffList);
			ff_cmb_staff.getSelectionModel().selectFirst();

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	private void onBtnSaveFixedFeeClicked(ActionEvent event) throws IOException {
		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			String query = "INSERT INTO tbl_fixed_fee(matter_no, activity_code, quantity, rate, date, amount, tax_status, tax_amount, total, billing_description, staff, billing,type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, DashboardController.temp_matter_no);
			pstmt.setString(2, ff_activity_code.getValue());
			pstmt.setString(3, ff_quantity.getText().trim());
			pstmt.setString(4, ff_rate.getText().trim());
			pstmt.setString(5, ff_date.getValue().toString());
			pstmt.setString(6, ff_amount.getText());
			pstmt.setString(7, ff_cmb_tax.getValue());
			pstmt.setString(8, ff_tax.getText());
			pstmt.setString(9, ff_total.getText());
			pstmt.setString(10, ff_description.getText().trim());
			pstmt.setString(11, ff_cmb_staff.getValue());
			pstmt.setString(12, ff_cmb_billing.getValue());
			pstmt.setString(13, "Fixed Fee");
			pstmt.execute();

			Statement stmt = conn.createStatement();
			ResultSet rs;
			stmt.execute("SELECT * from tbl_user_account WHERE matter_id=" + DashboardController.temp_matter_no + "");
			rs = stmt.getResultSet();
			if (rs.next()) {
				Double tempBalance = Double.parseDouble(rs.getString("balance"));
				switch (rs.getString("balance_type")) {
				case "CR": {
					if (tempBalance >= Double.parseDouble(ff_total.getText())) {
						query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr,type) VALUES ('"
								+ DashboardController.temp_matter_no + "','" + ff_date.getValue().toString() + "','"
								+ ff_tax.getText() + "','" + ff_amount.getText() + "','" + ff_description.getText()
								+ "','-','-','" + ff_total.getText() + "','-','FixedFee')";
						stmt.execute(query);

						stmt.execute("UPDATE tbl_user_account SET balance='"
								+ (tempBalance - Double.parseDouble(ff_total.getText())) + "' WHERE matter_id="
								+ DashboardController.temp_matter_no + "");
					} else {
						query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr,type) VALUES ('"
								+ DashboardController.temp_matter_no + "','" + ff_date.getValue().toString() + "','"
								+ ff_tax.getText() + "','" + ff_amount.getText() + "','" + ff_description.getText()
								+ "','" + ff_total.getText() + "','-','-','-','FixedFee')";
						stmt.execute(query);
					}

				}
					break;
				case "DR": {
					query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr,type) VALUES ('"
							+ DashboardController.temp_matter_no + "','" + ff_date.getValue().toString() + "','"
							+ ff_tax.getText() + "','" + ff_amount.getText() + "','" + ff_description.getText() + "','"
							+ ff_total.getText() + "','-','-','-','FixedFee')";
					stmt.execute(query);
				}
					break;
				default: {
				}
				}
			}

//			switch (ff_cmb_ledger_entry.getSelectionModel().getSelectedItem()) {
//
//			case "Office-DR": {
//				query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr,type) VALUES ('"
//						+ DashboardController.temp_matter_no + "','" + ff_date.getValue().toString() + "','"
//						+ ff_tax.getText() + "','" + ff_amount.getText() + "','" + ff_description.getText() + "','"
//						+ ff_total.getText() + "','-','-','-','FixedFee')";
//				stmt.execute(query);
//
//				/* Insert fixed fee tax into tbl_vat */
////				if (!ff_tax.getText().trim().isEmpty() && Double.parseDouble(ff_tax.getText().trim()) != 0) {
////					query = "SELECT MAX(ff_id) from tbl_fixed_fee WHERE matter_no='"
////							+ DashboardController.temp_matter_no + "'";
////					rs = stmt.executeQuery(query);
////					int temp_ff_id = 0;
////					if (rs.next()) {
////						temp_ff_id = rs.getInt(1);
////					}
////					query = "INSERT INTO tbl_vat(id, matter_id,type, date, description, vat_db, vat_cr) VALUES ('"
////							+ temp_ff_id + "','" + DashboardController.temp_matter_no + "','ff','"
////							+ ff_date.getValue().toString() + "','VAT','" + ff_tax.getText() + "','-')";
////					stmt.execute(query);
////				}
//			}
//				break;
//
//			case "Office-CR": {
//				query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr,type) VALUES ('"
//						+ DashboardController.temp_matter_no + "','" + ff_date.getValue().toString() + "','"
//						+ ff_tax.getText() + "','" + ff_amount.getText() + "','" + ff_description.getText() + "','-','"
//						+ ff_total.getText() + "','-','-','FixedFee')";
//				stmt.execute(query);
//
////				if (!ff_tax.getText().trim().isEmpty() && Integer.parseInt(ff_tax.getText().trim()) != 0) {
////					query = "SELECT MAX(ff_id) from tbl_fixed_fee WHERE matter_no='"
////							+ DashboardController.temp_matter_no + "'";
////					rs = stmt.executeQuery(query);
////					int temp_ff_id = 0;
////					if (rs.next()) {
////						temp_ff_id = rs.getInt(1);
////					}
////					query = "INSERT INTO tbl_vat(id, matter_id,type, date, description, vat_db, vat_cr) VALUES ('"
////							+ temp_ff_id + "','" + DashboardController.temp_matter_no + "','ff','"
////							+ ff_date.getValue().toString() + "','VAT','" + ff_tax.getText() + "','-')";
////					stmt.execute(query);
////				}
//
//			}
//				break;
//
//			case "Client-DR": {
//				query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr,type) VALUES ('"
//						+ DashboardController.temp_matter_no + "','" + ff_date.getValue().toString() + "','"
//						+ ff_tax.getText() + "','" + ff_amount.getText() + "','" + ff_description.getText()
//						+ "','-','-','" + ff_total.getText() + "','-','FixedFee')";
//				stmt.execute(query);
//
////				if (!ff_tax.getText().trim().isEmpty() && Integer.parseInt(ff_tax.getText().trim()) != 0) {
////					query = "SELECT MAX(ff_id) from tbl_fixed_fee WHERE matter_no='"
////							+ DashboardController.temp_matter_no + "'";
////					rs = stmt.executeQuery(query);
////					int temp_ff_id = 0;
////					if (rs.next()) {
////						temp_ff_id = rs.getInt(1);
////					}
////					query = "INSERT INTO tbl_vat(id, matter_id,type, date, description, vat_db, vat_cr) VALUES ('"
////							+ temp_ff_id + "','" + DashboardController.temp_matter_no + "','ff','"
////							+ ff_date.getValue().toString() + "','VAT','" + ff_tax.getText() + "','-')";
////					stmt.execute(query);
////				}
//
//			}
//				break;
//
//			case "Client-CR": {
//				query = "INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr,type) VALUES ('"
//						+ DashboardController.temp_matter_no + "','" + ff_date.getValue().toString() + "','"
//						+ ff_tax.getText() + "','" + ff_amount.getText() + "','" + ff_description.getText()
//						+ "','-','-','-','" + ff_total.getText() + "','FixedFee')";
//				stmt.execute(query);
//
////				if (!ff_tax.getText().trim().isEmpty() && Integer.parseInt(ff_tax.getText().trim()) != 0) {
////					query = "SELECT MAX(ff_id) from tbl_fixed_fee WHERE matter_no='"
////							+ DashboardController.temp_matter_no + "'";
////					rs = stmt.executeQuery(query);
////					int temp_ff_id = 0;
////					if (rs.next()) {
////						temp_ff_id = rs.getInt(1);
////					}
////					query = "INSERT INTO tbl_vat(id, matter_id,type, date, description, vat_db, vat_cr) VALUES ('"
////							+ temp_ff_id + "','" + DashboardController.temp_matter_no + "','ff','"
////							+ ff_date.getValue().toString() + "','VAT','" + ff_tax.getText() + "','-')";
////					stmt.execute(query);
////				}
//
//			}
//				break;
//			default: {
//
//			}
//				break;
//			}

			stmt.close();
			pstmt.close();
			conn.close();
			((Node) event.getSource()).getScene().getWindow().hide();

			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("../views/details_dashboard/DetailsDashboard.fxml"));
			Parent root = fxmlLoader.load();
			DetailsDashboardController ddc = fxmlLoader.getController();

			Scene scene = new Scene(root);
			DashboardController.tmpStage.setScene(scene);

			ddc.tabpane_time_fee.getSelectionModel().select(2);
			DashboardController.tmpStage.setIconified(true);
			DashboardController.tmpStage.setMaximized(true);
			DashboardController.tmpStage.show();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}
}