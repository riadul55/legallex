package legallexcrm.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import legallexcrm.DBConnection;
import legallexcrm.Models.DashboardMattersModel;

public class DashboardController implements Initializable {
	public static int temp_matter_no;//declaring matter number to access matter details 

	@FXML
	BorderPane dashboard_bp;

	@FXML
	private TableView<DashboardMattersModel> tv_matters_dashboard;

	@FXML
	private TableColumn<DashboardMattersModel, Integer> col_matter_no;

	@FXML
	private TableColumn<DashboardMattersModel, String> col_client;

	@FXML
	private TableColumn<DashboardMattersModel, String> col_matter_type;

	@FXML
	private TableColumn<DashboardMattersModel, String> col_respondent;

	@FXML
	private TableColumn<DashboardMattersModel, String> col_status;

	// Collections for importing data from database
	ObservableList<DashboardMattersModel> mattersList = FXCollections.observableArrayList();
	
	public static Stage tmpStage;
	public static int matter_id;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//Displaying Matters in Dash board
		DisplayMatters();
		
		tv_matters_dashboard.setRowFactory( tv -> {
		    TableRow<DashboardMattersModel> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
//		        	DashboardMattersModel rowData = row.getItem();
		        	temp_matter_no = row.getItem().getMatter_no();
		        	try {
		        		matter_id = row.getItem().getMatter_no(); // saving matter id when double clicked from dash board
		        	Stage stage = new Stage();
		    		stage.initStyle(StageStyle.UNDECORATED);
		    		Parent root = FXMLLoader.load(getClass().getResource("../views/details_dashboard/DetailsDashboard.fxml"));
		    		Scene scene = new Scene(root);
		    		stage.setScene(scene);
		    		stage.setMaximized(true);
		    		stage.show();
		    		tmpStage = stage;
		        	}catch(IOException e) {
		        		
		        	}
		        }
		    });
		    return row ;
		});
		
		//End of Initialize
	}

	@FXML
	private void onBtnCloseClicked(ActionEvent event) {

		((Node) event.getSource()).getScene().getWindow().hide();
		
	}

	@FXML
	private void onBtnMinimizeClicked(ActionEvent event) {

		((Stage) ((Button) event.getSource()).getScene().getWindow()).setIconified(true);

	}

	@FXML
	private void onBtnMaximizeClicked(ActionEvent event) {

		if (((Stage) ((Button) event.getSource()).getScene().getWindow()).isMaximized()) {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(false);
		} else {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(true);
		}

	}

	// Event Handler for "Home" in left side navigation in Dash board
	@FXML
	private void onBtnHomeClicked(ActionEvent event) throws IOException {

		Pane view = FXMLLoader.load(getClass().getResource("../views/side_over/side_over_home.fxml"));
		dashboard_bp.setCenter(null);
		dashboard_bp.setCenter(view);

	}

	// Event Handler for "Accounting" in left side navigation in Dash board
	@FXML
	private void onBtnAccountingClicked(ActionEvent event) throws IOException {

		Pane view = FXMLLoader.load(getClass().getResource("../views/side_over/side_over_accounting.fxml"));
		dashboard_bp.setCenter(null);
		dashboard_bp.setCenter(view);

	}

	// Event Handler for "Others" in left side navigation in Dash board
	@FXML
	private void onBtnOthersClicked(ActionEvent event) throws IOException {

		Pane view = FXMLLoader.load(getClass().getResource("../views/side_over/side_over_others.fxml"));
		dashboard_bp.setCenter(null);
		dashboard_bp.setCenter(view);

	}

	// Event Handler for "Settings" in left side navigation in Dash board
	@FXML
	private void onBtnSetttingsClicked(ActionEvent event) throws IOException {

		Pane view = FXMLLoader.load(getClass().getResource("../views/side_over/side_over_settings.fxml"));
		dashboard_bp.setCenter(null);
		dashboard_bp.setCenter(view);

	}

	// Event Handler for "New Matter" in Right Side Matters Panes
	@FXML
	private void onBtnNewMatterClicked(ActionEvent event) throws IOException {

		Pane view = FXMLLoader.load(getClass().getResource("../views/new_matter/nm_first.fxml"));
		dashboard_bp.setCenter(null);
		dashboard_bp.setCenter(view);

	}

	// Event Handler for "View Matters" in Right Side Matters Panes
	@FXML
	private void onBtnViewMattersClicked(ActionEvent event) throws IOException {

		Stage stg = (Stage) ((Button) event.getSource()).getScene().getWindow();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/Dashboard.fxml"));
		Parent root = loader.load();
		((Button) event.getSource()).getScene().setRoot(root);

	}

	// Event Handler for "New Matter" in Right Side Matters Panes
	@FXML
	private void onBtnNewCardClicked(ActionEvent event) throws IOException {

		Pane view = FXMLLoader.load(getClass().getResource("../views/new_card/Person.fxml"));
		dashboard_bp.setCenter(null);
		dashboard_bp.setCenter(view);

	}

	//Method Displaying Matters in Dash board Table View
	public void DisplayMatters() {
		mattersList.clear();
		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			Statement statement = conn.createStatement();
			//used for fetching client name from cards table
            statement.execute("SELECT * FROM tbl_matters WHERE user_id='"+LoginController.id+"'");
            
            ResultSet rs = statement.getResultSet();
            
            Statement stmt_card = conn.createStatement();      	

            while (rs.next()) {
            	stmt_card.execute("SELECT name FROM tbl_cards WHERE card_id='"+rs.getInt("card_id")+"'");
            	if(stmt_card.getResultSet().next()) {
            		mattersList.add(new DashboardMattersModel(
                            rs.getInt("id"),
                            stmt_card.getResultSet().getString(1),
                            rs.getString("matter_name"),
                            rs.getString("staff_p_resp"),
                            rs.getString("matter_status")));
            	}

            }
            rs.close();
			statement.close();
			conn.close();

		} catch (SQLException ex) {

		}
		
		col_matter_no.setCellValueFactory(new PropertyValueFactory<>("matter_no"));
		col_client.setCellValueFactory(new PropertyValueFactory<>("client"));
		col_matter_type.setCellValueFactory(new PropertyValueFactory<>("matter_type"));
		col_respondent.setCellValueFactory(new PropertyValueFactory<>("respondent"));
		col_status.setCellValueFactory(new PropertyValueFactory<>("status"));
		
		tv_matters_dashboard.setItems(mattersList);
	}
	
	
}
