package legallexcrm.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import legallexcrm.DBConnection;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

public class NMFirstController implements Initializable {

	public static int index = -1;
	public static String matter_title;
	public static String country;
	public static String client;
	public static String other_side;
	public static String other_side_solicitor;
	public static int chk_description = 0;
	public static String description;

	@FXML
	private TreeView<String> nmf_treeview;

	@FXML
	private ChoiceBox<String> cb_country;

	@FXML
	private ChoiceBox<String> cb_client;

	@FXML
	private ChoiceBox<String> cb_other_side;

	@FXML
	private ChoiceBox<String> cb_other_side_solicitor;

	@FXML
	private ChoiceBox<String> cb_other_side_insurer;

	@FXML
	private CheckBox chk_auto;

	@FXML
	private TextArea txt_area_description;

	@FXML
	private Button nm_first_btn_ok;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// loading TreeView Items
		LoadTreeView();

		// Disabling Description Text Area
		txt_area_description.setDisable(true);
		txt_area_description.setOpacity(0.5);

		// Disabling OK Button
		nm_first_btn_ok.setDisable(true);

		if (index > -1) {
			for (int i = 0; i < index; i++) {
				if (!nmf_treeview.getTreeItem(i).isLeaf()) {
					nmf_treeview.getTreeItem(i).setExpanded(true);
				}
			}
			nmf_treeview.getSelectionModel().select(index);
			cb_country.setValue(country);
			cb_client.setValue(client);
			cb_other_side.setValue(other_side);
			cb_other_side_solicitor.setValue(other_side_solicitor);
			cb_other_side_insurer.setValue("Not Applicable");
			nm_first_btn_ok.setDisable(false);
			txt_area_description.setText(description);

		}

		// Event Listener for Client Choice Box on Changed Value
		cb_client.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {

			if (!(cb_client.getSelectionModel().isEmpty())) {
				if (newValue.equals("Applicant")) {
					cb_client.getSelectionModel().select(0);
					cb_other_side.getSelectionModel().select(1);
					cb_other_side_solicitor.getSelectionModel().select(1);
				} else {
					cb_client.getSelectionModel().select(1);
					cb_other_side.getSelectionModel().select(0);
					cb_other_side_solicitor.getSelectionModel().select(0);
				}
			}

		});

		// Event Listener for Other Side Choice Box on Changed Value
		cb_other_side.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {

			if (!(cb_other_side.getSelectionModel().isEmpty())) {
				if (newValue.equals("Applicant")) {
					cb_client.getSelectionModel().select(1);
					cb_other_side.getSelectionModel().select(0);
					cb_other_side_solicitor.getSelectionModel().select(0);

				} else {
					cb_client.getSelectionModel().select(0);
					cb_other_side.getSelectionModel().select(1);
					cb_other_side_solicitor.getSelectionModel().select(1);
				}
			}

		});

		// Event Listener for Other Side SolicitorChoice Box on Changed Value
		cb_other_side_solicitor.getSelectionModel().selectedItemProperty()
				.addListener((options, oldValue, newValue) -> {

					if (!(cb_other_side.getSelectionModel().isEmpty())) {
						if (newValue.equals("Applicant`s Solicitor")) {
							cb_client.getSelectionModel().select(1);
							cb_other_side.getSelectionModel().select(0);
							cb_other_side_solicitor.getSelectionModel().select(0);

						} else {
							cb_client.getSelectionModel().select(0);
							cb_other_side.getSelectionModel().select(1);
							cb_other_side_solicitor.getSelectionModel().select(1);
						}
					}

				});

		chk_auto.setOnAction(event -> {
			if (chk_auto.isSelected()) {
				txt_area_description.setText(
						((TreeItem<?>) nmf_treeview.getSelectionModel().getSelectedItem()).getValue().toString());
				txt_area_description.setDisable(true);
				txt_area_description.setOpacity(0.5);
				chk_description = 0;
			} else {
				txt_area_description.setDisable(false);
				txt_area_description.setOpacity(1);
				chk_description = 1;
			}
		});

	}

	// Event Listener on Button[#nm_first_btn_ok].onAction
	@FXML
	public void onBtnNMFirstOkClicked(ActionEvent event) throws IOException, SQLException {
//		Connection conn = (Connection) new legallexcrm.DBConnection().DBConnect();
//		String query = "SELECT * from tbl_tmp_nmf";
//		Statement stmt = conn.createStatement();
//		ResultSet rs = stmt.executeQuery(query);
//
//		if (rs.next()) {
//
//		} else {
////			query = "INSERT INTO tbl_tmp_nmf VALUES(1,'"
////					+ (nmf_treeview.getSelectionModel().getSelectedItem()).getValue().toString() + "','"
////					+ cb_country.getValue().toString() + "','" + cb_client.getValue().toString() + "','"
////					+ cb_other_side.getValue().toString() + "','" + cb_other_side_solicitor.getValue().toString() + "','"
////					+ txt_area_description.getText().trim() + "')";
//			query = "INSERT INTO tbl_tmp_nmf VALUES("+LoginController.id+",'"
//					+ (nmf_treeview.getSelectionModel().getSelectedItem()).getValue().toString() + "','"
//					+ cb_country.getValue().toString() + "','" + cb_client.getValue().toString() + "','"
//					+ cb_other_side.getValue().toString() + "','" + cb_other_side_solicitor.getValue().toString()
//					+ "','" + txt_area_description.getText().trim() + "')";
//
//			stmt.executeUpdate(query);
//
//		}
//		conn.close();

		index = nmf_treeview.getSelectionModel().getSelectedIndex();
		matter_title = (nmf_treeview.getSelectionModel().getSelectedItem()).getValue();
		country = cb_country.getValue().toString();
		client = cb_client.getValue().toString();
		other_side = cb_other_side.getValue().toString();
		other_side_solicitor = cb_other_side_solicitor.getValue().toString();
		description = txt_area_description.getText().trim();

		Stage stg = (Stage) ((Button) event.getSource()).getScene().getWindow();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/Dashboard.fxml"));
		Parent root = loader.load();
		((Button) event.getSource()).getScene().setRoot(root);

		DashboardController dbc = loader.getController();
		dbc.dashboard_bp.setCenter(FXMLLoader.load(getClass().getResource("../views/new_matter/nm_second.fxml")));

	}

	@FXML
	public void onNMFTreeViewClicked(MouseEvent event) throws IOException {

		if (!(nmf_treeview.getSelectionModel().isEmpty())) {
			if (((TreeItem<?>) nmf_treeview.getSelectionModel().getSelectedItem()).getValue().equals("Adoptation")) {

				// Setting Country Choice box Values
				cb_country.getItems().clear();
				cb_country.getItems().add("UK");
				cb_country.getSelectionModel().select(0);

				// Setting Client Choice box Values
				cb_client.getItems().clear();
				cb_client.getItems().addAll("Applicant", "Respondent");
				cb_client.getSelectionModel().select(0);

				// Setting Other Side Choice box Values
				cb_other_side.getItems().clear();
				cb_other_side.getItems().addAll("Applicant", "Respondent");
				cb_other_side.getSelectionModel().select(1);

				// Setting Other Side Choice box Values
				cb_other_side_solicitor.getItems().clear();
				cb_other_side_solicitor.getItems().addAll("Applicant`s Solicitor", "Respondent`s Solicitor");
				cb_other_side_solicitor.getSelectionModel().select(1);

				// Setting Other Side Choice box Values
				cb_other_side_insurer.getItems().clear();
				cb_other_side_insurer.getItems().add("Not Applicable");
				cb_other_side_insurer.getSelectionModel().select(0);
				cb_other_side_insurer.setDisable(true);
				cb_other_side_insurer.setOpacity(1.0);

				txt_area_description.setText("Adoptation");
				nm_first_btn_ok.setDisable(false);
			}
		}

	}

	@SuppressWarnings("unchecked")
	public void LoadTreeView() {

		/*----------------Family Law & Children Tree Items--------------*/

		TreeItem<String> root = new TreeItem<String>("Root");
		nmf_treeview.setRoot(root);
		nmf_treeview.setShowRoot(false);

		TreeItem<String> familyLaw = new TreeItem<String>("Family Law");
		root.getChildren().add(familyLaw);

		TreeItem<String> children = new TreeItem<String>("Children");
		familyLaw.getChildren().addAll(children);

		TreeItem<String> adoptation = new TreeItem<String>("Adoptation");

		TreeItem<String> care_proceedings = new TreeItem<String>("Care Proceedings");

		TreeItem<String> private_child_arrangements = new TreeItem<String>("Private Child Arrangements");

		children.getChildren().addAll(adoptation, care_proceedings, private_child_arrangements);

		/*-------------Family Law Regarding Matrimonial--------------------------*/

		TreeItem<String> matrimonial = new TreeItem<String>("Matrimonial");
		familyLaw.getChildren().addAll(matrimonial);

		TreeItem<String> civil_proceedings = new TreeItem<String>("Civil Proceedings");

		TreeItem<String> divorce_dissolutions = new TreeItem<String>("Divorce & Dissolutions");

		TreeItem<String> pre_post_nuptial = new TreeItem<String>("Pre & Post Nuptial Agreements");

		matrimonial.getChildren().addAll(civil_proceedings, divorce_dissolutions, pre_post_nuptial);

		/*-------------Family Law Regarding Others--------------------------*/

		TreeItem<String> family_others = new TreeItem<String>("Others");
		familyLaw.getChildren().addAll(family_others);

		TreeItem<String> others_family = new TreeItem<String>("Family");

		TreeItem<String> others_financial = new TreeItem<String>("Financial");

		TreeItem<String> domestic_voilence = new TreeItem<String>("Domestic Violence");

		TreeItem<String> mediation = new TreeItem<String>("Mediation");

		family_others.getChildren().addAll(others_family, others_financial, domestic_voilence, mediation);

		/*-------------Criminal Law Tree Items--------------------------*/

		TreeItem<String> criminal_law = new TreeItem<String>("Criminal Law");
		root.getChildren().add(criminal_law);

		TreeItem<String> criminal_general = new TreeItem<String>("General");
		criminal_law.getChildren().addAll(criminal_general);

		TreeItem<String> criminal = new TreeItem<String>("Criminal");

		TreeItem<String> motoring_offences = new TreeItem<String>("Motoring Offences");

		TreeItem<String> criminal_crown_court = new TreeItem<String>("Criminal Crown Court");

		criminal_general.getChildren().addAll(criminal, motoring_offences, criminal_crown_court);

		/*-------------Litigation Law Tree Items--------------------------*/

		TreeItem<String> litigation = new TreeItem<String>("Litigation");
		root.getChildren().add(litigation);

		TreeItem<String> litigation_cost = new TreeItem<String>("Costs");

		TreeItem<String> litigation_property_disputes = new TreeItem<String>("Property Disputes");

		litigation.getChildren().addAll(litigation_cost, litigation_property_disputes);

		TreeItem<String> debt_recovery = new TreeItem<String>("Debt Recovery");

		TreeItem<String> enforcement = new TreeItem<String>("Enforcement");

		TreeItem<String> general_litigation = new TreeItem<String>("General Litigation");

		TreeItem<String> professional_negligence = new TreeItem<String>("Professional Negligence");

		litigation_cost.getChildren().addAll(debt_recovery, enforcement, general_litigation, professional_negligence);
		
		/*-----------------Litigation Property Disputes---------------------*/

		TreeItem<String> boundary_disputes = new TreeItem<String>("Boundary Dispute");

		TreeItem<String> freehold_property_disputes = new TreeItem<String>("Freehold Property Dispute");

		TreeItem<String> leasehold_property_disputes = new TreeItem<String>("Leasehold Property Dispute");

		TreeItem<String> tenancy_disputes = new TreeItem<String>("Tenancy Dispute");

		litigation_property_disputes.getChildren().addAll(boundary_disputes,freehold_property_disputes,leasehold_property_disputes,tenancy_disputes);

	}
}
