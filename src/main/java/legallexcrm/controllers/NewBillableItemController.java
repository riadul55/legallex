package legallexcrm.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import legallexcrm.DBConnection;
import legallexcrm.Models.HourlyFeeInnerModel;

public class NewBillableItemController implements Initializable {
	@FXML
	private TextField hf_matter_no;

	@FXML
	private ComboBox<String> hf_cmb_staff;

	@FXML
	private ComboBox<String> hf_cmb_billing_stage;

	@FXML
	private ComboBox<String> hf_cmb_activity_rate;

	@FXML
	private DatePicker hf_date;

	@FXML
	private ComboBox<String> hf_cmb_hearing_type;

	@FXML
	private ComboBox<String> hf_cmb_attended_on;

	// Items to be stored in Table View

	@FXML
	private ComboBox<String> hf_cmb_activity;

	@FXML
	private ComboBox<String> hf_cmb_tax;

	@FXML
	private TextField hf_rate;

	@FXML
	private TextField hf_units;

	@FXML
	private TextField hf_amount;

	@FXML
	private TextField hf_tax_amount;

	@FXML
	private TextField hf_total_amount;

	@FXML
	private TextArea hf_description;

	// Table View Attributes

	@FXML
	private TableView<HourlyFeeInnerModel> tv_hourly_fee;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_activity;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_description;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_tax;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_rate;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_units;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_amount;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_tax_amount;

	@FXML
	private TableColumn<HourlyFeeInnerModel, String> col_total_amount;

	ObservableList<HourlyFeeInnerModel> temphf = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		listStaff();
		hf_matter_no.setText(String.valueOf(DashboardController.temp_matter_no));
		hf_date.setValue(LocalDate.now());

		// Temporary values for combo boxes
		hf_cmb_billing_stage.getItems().add("Billing Stage");
		hf_cmb_billing_stage.getSelectionModel().selectFirst();

		hf_cmb_activity_rate.getItems().add("Activity Rate");
		hf_cmb_activity_rate.getSelectionModel().selectFirst();

		hf_cmb_hearing_type.getItems().add("Hearing Type");
		hf_cmb_hearing_type.getSelectionModel().selectFirst();

		hf_cmb_attended_on.getItems().add("Attended On");
		hf_cmb_attended_on.getSelectionModel().selectFirst();

		hf_cmb_activity.getItems().add("activity");
		hf_cmb_activity.getSelectionModel().selectFirst();

		hf_cmb_tax.getItems().addAll("VAT Included", "No VAT");
		hf_cmb_tax.getSelectionModel().selectFirst();

		/*----------------Decimal Pattern for Fee rates, amount, tax, total----------------------*/

		Pattern decimalPattern = Pattern.compile("\\d*(\\.\\d{0,2})?");

		UnaryOperator<TextFormatter.Change> decimal = c -> {
			if (decimalPattern.matcher(c.getControlNewText()).matches()) {
				return c;
			} else {
				return null;
			}
		};

		hf_rate.setTextFormatter(new TextFormatter<>(decimal));
		hf_amount.setTextFormatter(new TextFormatter<>(decimal));
		hf_tax_amount.setTextFormatter(new TextFormatter<>(decimal));
		hf_total_amount.setTextFormatter(new TextFormatter<>(decimal));
		hf_units.setTextFormatter(new TextFormatter<>(decimal));

		/*-----------Listener for Rate-----------------*/
		hf_rate.textProperty().addListener((observable, oldValue, newValue) -> {

			if (hf_rate.getText().trim().isEmpty() || Double.parseDouble(hf_rate.getText().trim()) == 0) {
				hf_amount.clear();
				hf_tax_amount.clear();
				hf_total_amount.clear();
			} else {
				if (!hf_units.getText().trim().isEmpty() && Double.parseDouble(hf_units.getText().trim()) != 0) {
					hf_amount.setText(Double.toString(Integer.parseInt(hf_units.getText().trim())
							* Double.parseDouble(hf_rate.getText().trim())));
					if (hf_cmb_tax.getSelectionModel().getSelectedIndex() == 0) {
						hf_tax_amount
								.setText(Double.toString((Double.parseDouble(hf_amount.getText().trim()) * 20) / 100));
						hf_total_amount.setText(Double.toString((Double.parseDouble(hf_amount.getText().trim())
								+ Double.parseDouble(hf_tax_amount.getText().trim()))));
					} else {
						hf_total_amount.setText(hf_amount.getText().trim());
					}
				}
			}

		});

		hf_units.textProperty().addListener((observable, oldValue, newValue) -> {

			if (hf_units.getText().trim().isEmpty() || Double.parseDouble(hf_units.getText().trim()) == 0) {
				hf_amount.clear();
				hf_tax_amount.clear();
				hf_total_amount.clear();
			} else {
				if (!hf_rate.getText().trim().isEmpty() && Double.parseDouble(hf_rate.getText().trim()) != 0) {
					hf_amount.setText(Double.toString(Integer.parseInt(hf_units.getText().trim())
							* Double.parseDouble(hf_rate.getText().trim())));
					if (hf_cmb_tax.getSelectionModel().getSelectedIndex() == 0) {
						hf_tax_amount
								.setText(Double.toString((Double.parseDouble(hf_amount.getText().trim()) * 20) / 100));
						hf_total_amount.setText(Double.toString((Double.parseDouble(hf_amount.getText().trim())
								+ Double.parseDouble(hf_tax_amount.getText().trim()))));
					} else {
						hf_total_amount.setText(hf_amount.getText().trim());
					}
				}
			}

		});

		/* Combo Box listener for tax status */
		hf_cmb_tax.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
			if (hf_cmb_tax.getSelectionModel().getSelectedIndex() == 1) {
				hf_tax_amount.clear();
				if ((!hf_units.getText().trim().isEmpty() && Double.parseDouble(hf_units.getText().trim()) != 0)
						&& (!hf_rate.getText().trim().isEmpty() && Double.parseDouble(hf_rate.getText().trim()) != 0)) {
					hf_amount.setText(Double.toString(Double.parseDouble(hf_units.getText().trim())
							* Double.parseDouble(hf_rate.getText().trim())));
					hf_total_amount.setText(hf_amount.getText());
				} else {
					hf_total_amount.clear();
					hf_amount.clear();
				}
			} else {
				if ((!hf_units.getText().trim().isEmpty() && Double.parseDouble(hf_units.getText().trim()) != 0)
						&& (!hf_rate.getText().trim().isEmpty() && Double.parseDouble(hf_rate.getText().trim()) != 0)) {
					hf_amount.setText(Double.toString(Double.parseDouble(hf_units.getText().trim())
							* Double.parseDouble(hf_rate.getText().trim())));
					hf_tax_amount.setText(Double.toString((Double.parseDouble(hf_amount.getText().trim()) * 20) / 100));
					hf_total_amount.setText(Double.toString((Double.parseDouble(hf_amount.getText().trim())
							+ Double.parseDouble(hf_tax_amount.getText().trim()))));

				} else {
					hf_total_amount.clear();
					hf_amount.clear();
					hf_tax_amount.clear();
				}

			}
		});

		// Assigning table view column values
		col_activity.setCellValueFactory(new PropertyValueFactory<>("activity"));
		col_description.setCellValueFactory(new PropertyValueFactory<>("billing_description"));
		col_tax.setCellValueFactory(new PropertyValueFactory<>("tax"));
		col_rate.setCellValueFactory(new PropertyValueFactory<>("rate"));
		col_units.setCellValueFactory(new PropertyValueFactory<>("units"));
		col_amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
		col_tax_amount.setCellValueFactory(new PropertyValueFactory<>("tax_amount"));
		col_total_amount.setCellValueFactory(new PropertyValueFactory<>("total_amount"));

	}

	@FXML
	private void onBtnCloseClicked(ActionEvent event) {

		((Node) event.getSource()).getScene().getWindow().hide();

	}

	@FXML
	private void onBtnMinimizeClicked(ActionEvent event) {

		((Stage) ((Button) event.getSource()).getScene().getWindow()).setIconified(true);

	}

	@FXML
	private void onBtnMaximizeClicked(ActionEvent event) {

		if (((Stage) ((Button) event.getSource()).getScene().getWindow()).isMaximized()) {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(false);
		} else {
			((Stage) ((Button) event.getSource()).getScene().getWindow()).setMaximized(true);
		}
	}

	public void listStaff() {
		ObservableList<String> staffList = FXCollections.observableArrayList();
		staffList.clear();
		Connection conn;
		try {
			conn = (Connection) new DBConnection().DBConnect();
			String query = "SELECT * from tbl_staff";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				staffList.add(rs.getString("staff_name"));
			}

			hf_cmb_staff.setItems(staffList);
			hf_cmb_staff.getSelectionModel().selectFirst();

			stmt.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	private void onBtnAddClicked(ActionEvent event) {
		// Insert data temporarily into model
		temphf.add(new HourlyFeeInnerModel(DashboardController.temp_matter_no, hf_cmb_activity.getValue().toString(),
				hf_description.getText(), hf_cmb_tax.getValue().toString(), hf_rate.getText().trim(),
				hf_units.getText().trim(), hf_amount.getText(), hf_tax_amount.getText(), hf_total_amount.getText()));
		tv_hourly_fee.setItems(temphf);

		// Clear Filed after data inserted into temporary model
		hf_rate.clear();
		hf_units.clear();
		hf_tax_amount.clear();
		hf_amount.clear();
		hf_total_amount.clear();
		hf_description.clear();

	}

	@FXML
	private void onBtnSaveClicked(ActionEvent event) throws IOException {

		Connection conn;
		String query = "INSERT INTO tbl_hourly_fee(matter_no, staff, billing_stage, activity_rate, date, hearing_type, attended_on, activity, billing_description, tax, rate, units, amount, tax_amount, total_amount,type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			conn = (Connection) new DBConnection().DBConnect();
			PreparedStatement pstmt;

			for (HourlyFeeInnerModel hf_data : temphf) {
//				query = "INSERT INTO tbl_fixed_fee(matter_no, staff, billing_stage, activity_rate, date, hearing_type, attended_on, activity, billing_description, tax, rate, units, amount) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
				
				pstmt = conn.prepareStatement(query);
				
				pstmt.setInt(1, DashboardController.temp_matter_no);
				pstmt.setString(2, hf_cmb_staff.getValue());
				pstmt.setString(3, hf_cmb_billing_stage.getValue().toString());
				pstmt.setString(4, hf_cmb_activity_rate.getValue());
				pstmt.setString(5, hf_date.getValue().toString());
				pstmt.setString(6, hf_cmb_hearing_type.getValue());
				pstmt.setString(7, hf_cmb_attended_on.getValue());
				pstmt.setString(8, hf_data.getActivity());
				pstmt.setString(9, hf_data.getBilling_description());
				pstmt.setString(10, hf_data.getTax());
				pstmt.setString(11, hf_data.getRate());
				pstmt.setString(12, hf_data.getUnits());
				pstmt.setString(13, hf_data.getAmount());
				pstmt.setString(14, hf_data.getTax_amount());
				pstmt.setString(15, hf_data.getTotal_amount());
				pstmt.setString(16, "Hourly Fee");

				pstmt.execute();
				pstmt.close();

				Statement stmt = conn.createStatement();
				stmt.execute(
						"INSERT INTO tbl_invoices(matter_id, date, tax_amount, total_amount, description, office_db, office_cr, client_db, client_cr, type) VALUES ('"
								+ DashboardController.temp_matter_no + "','" + hf_date.getValue().toString() + "','"
								+ hf_data.getTax_amount() + "','-','" + hf_data.getBilling_description()
								+ "','-','-','-','" + hf_data.getTotal_amount() + "','HourlyFee')");
				stmt.close();

			}
			conn.close();
			((Node) event.getSource()).getScene().getWindow().hide();

			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("../views/details_dashboard/DetailsDashboard.fxml"));
			Parent root = fxmlLoader.load();
			DetailsDashboardController ddc = fxmlLoader.getController();

			Scene scene = new Scene(root);
			DashboardController.tmpStage.setScene(scene);

			ddc.tabpane_time_fee.getSelectionModel().select(2);
			DashboardController.tmpStage.setIconified(true);
			DashboardController.tmpStage.setMaximized(true);
			DashboardController.tmpStage.show();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

}