package legallexcrm.controllers;


import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import legallexcrm.DBConnection;

public class LoginController implements Initializable {
	
	public static int id;//stores user id
	public static String username;//stores account email as user name
	
	
	@FXML
	private TextField txt_username;
	
	@FXML
	private TextField txt_password;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	@FXML
	private void onBtnCloseClicked(ActionEvent event) {

		((Node) event.getSource()).getScene().getWindow().hide();

	}

	@FXML
	private void onBtnLoginClicked(ActionEvent event) throws IOException {
		LoginCheck();
		((Node) event.getSource()).getScene().getWindow().hide();
		Stage stage = new Stage();
//		stage.initStyle(StageStyle.UNDECORATED);
		stage.initStyle(StageStyle.UNDECORATED);
		Parent root = FXMLLoader.load(getClass().getResource("../views/Dashboard.fxml"));
		Scene scene = new Scene(root);
//		scene.setFill(Color.TRANSPARENT);
		stage.setScene(scene);
		stage.setMaximized(true);
//        stage.getIcons().add(new Image("/medic/user-icon.png"));
		stage.setTitle("Legallex CRM");
		stage.show();
	}
	
	public void LoginCheck() {
		
		try {
			Connection conn = (Connection) new DBConnection().DBConnect();
			PreparedStatement pst = conn.prepareStatement("select * from tbl_users where acc_email=? and acc_password=?");
            pst.setString(1, txt_username.getText().trim());
            pst.setString(2,txt_password.getText().trim());
            ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				id=rs.getInt("id");
				username = rs.getString("acc_email");
			}else {
				System.out.println("Nothing Found");
			}
			conn.close();
			}catch (SQLException ex) {
                System.out.print(ex);
            }
			
	}
	
}
