package legallexcrm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    String url="jdbc:mysql://localhost/dblegallexcrm";
    String username="root";
    String password="";

    public Connection DBConnect() throws SQLException {
        Connection conn = DriverManager.getConnection(url,username,password);
        return conn;
    }
}
